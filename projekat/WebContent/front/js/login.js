$(document).ready(function() {
	if (localStorage.getItem("user")){
		$(location).attr('href',"/WebProjekat/front/profile.html");
	}

	$('#loginForm').submit(function(event) {
		event.preventDefault();
		let username = $('#username').val();
		let password = $('#password').val();
		if(!checkInputs(username, password)){
			return;
		}

		$.post({
			url: '../rest/LoginService/login',
			data: JSON.stringify({username: username, 
								  password: password}),
			contentType: 'application/json',
			success: function(user) {
				storeLoggedUser(user);
				$(location).attr('href',"/WebProjekat/front/profile.html");
			},
			error: function(message) {
				$('#error').text(message.responseText);
				$("#error").show().delay(2000).fadeOut();
			}
		});
	});
});

function checkInputs(username, password){
	var isValid = true;
	if(!username){
		$('#usernameError').text("- Please input your username.");
		$('#usernameError').show().delay(2000).fadeOut();
		isValid = false;
	}
	
	if(!password){
		$('#passwordError').text("- Please input your password.");
		$('#passwordError').show().delay(2000).fadeOut();
		isValid = false;
	}
	return isValid;
}