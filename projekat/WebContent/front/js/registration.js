$(document).ready(function() {
	if (localStorage.getItem("user")){
		$(location).attr('href',"/WebProjekat/front/profile.html");
	}
	
	$('#reset').click(function(){
		$('#firstName').val("");
		$('#lastName').val("");
		$("#birthdate").val("");
		$('#email').val("");
		$('#username').val("");
		$('#password').val("");
		$('#ReTypePassword').val("");
	});
	
	$('#registrationForm').submit(function(event) {
		event.preventDefault();
		let firstName = $('#firstName').val();
		let lastName = $('#lastName').val();
		let date = $('#birthdate').val();
		let email = $('#email').val();
		let sex = $("input[type='radio'][name='gender']:checked").val();
		let username = $('#username').val();
		let password = $('#password').val();
		let ReTypePassword = $('#ReTypePassword').val();
	
		if(!checkInputs(firstName, lastName, date, email, username, password, ReTypePassword)){
			return;
		}
		if(ReTypePassword!== password){
			$('#ReTypePasswordError').text("Passwords must match");
			$("#ReTypePasswordError").show().delay(2000).fadeOut();
			return;
		}
		$('#error').hide();
		$.post({
			url: '../rest/RegistrationService/register',
			data: JSON.stringify({firstName: firstName, 
								  lastName: lastName,
								  email: email,
								  sex: sex,
								  birthDate : date, 
								  username: username, 
								  password: password}),
			contentType: 'application/json',
			success: function(user) {
				storeLoggedUser(user); 
				window.location.replace("../front/profile.html");
			},
			error: function(message) {
				console.log(message.responseText);
				$('#error').text(message.responseText);
				$("#error").show().delay(2000).fadeOut();
			}
		});
	});
});

function checkInput(input, errorLabelId, errorMessageSuffix){
	if(!input){
		writeErrorLabel(errorLabelId, errorMessageSuffix);

		return false;
	}

	return true
}

function writeErrorLabel(errorLabelId, errorMessageSuffix){
	$('#' + errorLabelId).text("- Please input your " + errorMessageSuffix + ".");
	$('#' + errorLabelId).show().delay(2000).fadeOut();
}

function checkEmail(email){
	let notEmpty = checkInput(email, "emailError", "email");
	let validFormat = validateEmail(email);

	if (notEmpty && !validFormat){
		writeErrorLabel("emailError", "email in required format");
	}

	return notEmpty & validFormat;
}

function checkDateOfBirth(date){
	let notEmpty = checkInput(date, "birthdateError", "date");
	date = date.replace("/", ".");
	date = date.replace("/", ".");
	let validFormat = validateDate(date);

	if (notEmpty && !validFormat){
		writeErrorLabel("birthdateError", "date in required format");
	}

	return notEmpty & validFormat;
}

function checkPasswordReTypedCorrectly(password, retypedPassword){
	return password === retypedPassword;
}

function checkInputs(firstName, lastName, date, email, username, password, retypedPassword){
	//TODO validate that names dont contain digits
	let isValid = checkInput(firstName, "firstNameError", "name");
	isValid &= checkInput(lastName, "lastNameError", "last name");
	isValid &= checkDateOfBirth(date);
	isValid &= checkEmail(email);
	isValid &= checkInput(username, "usernameError", "username");
	isValid &= checkInput(password, "passwordError", "password");
	isValid &= checkInput(retypedPassword, "ReTypePasswordError", "re-typed password");
	isValid &= checkPasswordReTypedCorrectly(password, retypedPassword);
	
	return isValid;
}
