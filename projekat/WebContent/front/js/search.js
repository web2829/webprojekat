function getUserRowHtml(user){
    let fullName = user.firstName + " " + user.lastName;

    return `
        <div class="user row post mt-4 pt-2 pb-2" id="foundUser_${user.username}">
            <div class="row user_info d-flex align-items-center">
                <div class="col-sm-2">
                    <img src="../rest/ImageService/getImg/${user.profilePicture}" alt="user profile photo" class="rounded-circle" id="user_profile_photo">
                </div>
                <div class="col-sm-4 ">
                    <p id="user_fullname">${fullName}</p>
                </div>
                <div class="col-sm-4">
                    <p id="user_birthdate">${user.birthDate}</p>
                </div>
                <div class="col-sm-auto" align="right">
                </div>
            </div>
        </div>
    `;
}

function validateName(name){
    return validateFilledInput(name, isAllLetters, "nameErrorLabel");
}

function validateLastName(lastname){
    return validateFilledInput(lastname, isAllLetters, "lastNameErrorLabel");
}

function validateFromBirthdate(date){
    return validateFilledInput(date, validateDate, "birthdateErrorLabel");
}

function validateToBirthdate(date){
    return validateFilledInput(date, validateDate, "birthdateErrorLabel");
}

function validateEmail(email){
    return validateFilledInput(email, validateEmail, "emailErrorLabel");
}

function validateFilledInput(input, validateFunc, errorLabelId){
    if (input !== ""){
        if (!validateFunc(input)){
            writeErrorLabel(errorLabelId);
            return false;
        }

        return true
    }

    return false;
}

function writeErrorLabel(errorLabelId, message="Invalid input"){
	$('#' + errorLabelId).text(message);
	$('#' + errorLabelId).show().delay(2000).fadeOut();
}

function loadUserRows(users){
    let usersDiv = $("#users");
    usersDiv.empty();

    if (users.length != 0){
        for (let i = 0; i < users.length; i++){
			$(usersDiv).append(getUserRowHtml(users[i]));
			addProfileListener(users[i]);	
    	}
    } else{
        $(usersDiv).append("<h3>No results</h3>");
    }  
}

function addProfileListener(user){
	$("#foundUser_" + user.username).click(function(){
           $(location).attr('href',"/WebProjekat/front/profile.html?" + user.username);
    });
}

function addSearchInputListenerOneField(inputId, validateFunc, urlPostfix){
    let inputField = $("#" + inputId);

    $(inputField).keypress(function(e){
        let key = e.which;

        if (key == 13){ //enter key
            let inputVal = $(inputField).val();

            if (validateFunc(inputVal)){
                let sortType = getSortType();

                $.get({
                    url: '../rest/UserService/' + urlPostfix + "/" + inputVal + "/" + sortType,
                    contentType: 'application/json',
                    success: function(users) {
                        loadUserRows(users);
                    }
                });
            }
        }
    });
}

function addSearchInputListenerDates(){
    let fromBirthdateInput = $("#fromBirthdateInput");
    let toBirthdateInput = $("#toBirthdateInput");
    addSearchInputListenerDate(fromBirthdateInput);
    addSearchInputListenerDate(toBirthdateInput);
}

function addSearchInputListenerDate(dateInputField){
    $(dateInputField).keypress(function(e){
        let key = e.which;

        if (key == 13){ //enter key
            let fromBirthdate = $("#fromBirthdateInput").val();
            let toBirthdate = $("#toBirthdateInput").val();
			fromBirthdate = fromBirthdate.replace("/", ".");
			fromBirthdate = fromBirthdate.replace("/", ".");
			toBirthdate = toBirthdate.replace("/", ".");
			toBirthdate = toBirthdate.replace("/", ".");

            if (validateFromBirthdate(fromBirthdate) && validateToBirthdate(toBirthdate)){
                let sortType = getSortType();

                $.get({
                    url: '../rest/UserService/searchBirthdate/' + fromBirthdate + "/" + toBirthdate + "/" + sortType,
                    contentType: 'application/json',
                    success: function(users) {
                        loadUserRows(users);
                    }
                });
            }
        }
    });
}

function addAllUsersButtonListener(){
    $("#showAllUsersAdminButton").click(function(){
        let sortType = getSortType();

        $.get({
            url: '../rest/UserService/searchAll/' + sortType,
            contentType: 'application/json',
            success: function(users) {
                loadUserRows(users);
            }
        });     
    });
}

function getSortType(){
    return $("#sortForm input[type='radio']:checked").val();
}

$(document).ready(function() {
    addSearchInputListenerOneField("nameInput", validateName, "searchName");
    addSearchInputListenerOneField("lastNameInput", validateLastName, "searchLastName");
    addSearchInputListenerDates();

    if(loadLoggedUser() && loadLoggedUser().role == "ADMIN"){
        addSearchInputListenerOneField("emailInput", validateEmail, "searchEmail");
        addAllUsersButtonListener();
    } else {
        $(".searchByEmail").remove();
        $(".showAllUsersButton").remove();
    }
    
});