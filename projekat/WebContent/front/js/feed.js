$(document).ready(function() {
    addNewPostListener(loadLoggedUser());
    let username = loadLoggedUser().username;

    $.get({
        url: '../rest/PostService/feed/' + username,
        contentType: 'application/json',
        success: function(friendsPosts) {
            loadPosts(friendsPosts);
        }
    });
});