function loadSentRequests(sentRequests, elemName, sent){
	let element = $(elemName);
	if(sentRequests.length==0){
		element.append(`</br></br><h3 align="center"> No requests</h3>`);
	}
    for (let i = 0; i < sentRequests.length; i++){
		loadSentRequest(sentRequests[i], element, sent);
    }

	element.scrollTop(element.prop("scrollHeight"));
}


function loadSentRequest(request, element, sent) {
	let username = request.senderUsername;
	if(sent){
		username = request.recieverUsername;
	}
	
	console.log("'../rest/UserService/user/'" + username)

	$.get({
		url: '../rest/UserService/user/' + username,
		contentType: 'application/json',
		success: function(reciever){
			element.append(getHtmlMessage(reciever, request, sent));
			addListeners(request, username, sent, element);
		}
	});
	
  
    element.scrollTop(element.prop("scrollHeight"));
}

function addListeners(request, username, sent, element){
	if(sent){
		CancelRequestListener(request, "#cancel_"+username, element, "cancelRequest");
	}
	else{
		CancelRequestListener(request, "#accept_"+username, element, "acceptRequest");
		CancelRequestListener(request, "#reject_"+username, element, "rejectRequest");
	}
}

function CancelRequestListener(request, button, element, operation){
    $(button).click(function(){
		if (!window.confirm('Are you sure you want to ' + operation + '?'))
		{ return; }
		$.post({
			url: '../rest/RequestService/' + operation,
			data: JSON.stringify({requestID: request.requestID,
								  senderUsername: request.senderUsername,
								  recieverUsername: request.recieverUsername,
								  status: request.status,
								  date: request.date
								  }),
			contentType: 'application/json',
			success: function(){ 
				$("#request_id_"+request.requestID).remove();				
			},
			error: function(){
				alert("Oops, something went wrong!");	
			}
		});
    })
}


$(document).ready(function() {
	
	loggedUser = loadLoggedUser();
		
	$.get({
		url: '../rest/UserService/user/' + loggedUser.username,
		contentType: 'application/json',
		success: function(user){
			loadSentRequests(user.pendingSentFriendRequests, ".sent_requests", true);
			loadSentRequests(user.pendingRecievedFriendRequests, ".recieved_requests", false);
		}
	});
	
});

let loggedUser;

function getHtmlMessage(user, request, sent){
	
    var firstPart = `
			<div class="user row mt-4 pt-2 pb-2" id="request_id_${request.requestID}">
				<div class="row  d-flex align-items-center">
                    <div class="col-sm-3" align="left">
                        <p id="post_timestamp" style="color:grey;  font-size: 12px;">${request.date}</p> 
                    </div>
                </div>
		        <div class="row user_info d-flex align-items-center">
		            <div class="col-sm-2">
		                <img src="../rest/ImageService/getImg/${user.profilePicture}" alt="user profile photo" class="rounded-circle" id="user_profile_photo">
		            </div>
		            <div class="col-sm-4 ">
						<a href="/WebProjekat/front/profile.html?${user.username}" style="color: black; text-decoration: none">
		                	<p id="user_fullname">${user.firstName} ${user.lastName}</p>
						</a>
		            </div>
		            <div class="col-sm-1">
		            </div>
		            <div class="col-sm-auto" align="right">`;
	var middlePart = `      <button id="cancel_${user.username}" class="btn btn-warning pull-right ignore_button">Cancel</button>`;
	if(!sent){
		middlePart = `
                            <button id="accept_${user.username}" class="btn btn-warning pull-right accept_button">Accept</button>
                            <button id="reject_${user.username}" class="btn btn-warning pull-right ignore_button">Reject</button>`;
	}	
	
	var secondPart=   `</div>
                    </div>
                </div>
    `;				
	return firstPart + middlePart +secondPart; 
	
}
