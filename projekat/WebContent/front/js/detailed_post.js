function getEditTextarea(currentText, id){
    return `<textarea id="${id}" class="form-control input-lg p-text-area">${currentText}</textarea>`;
}

function getReasonTextarea(id){
    return `<textarea id="${id}" class="form-control input-lg p-text-area"></textarea>`;
}

function getP(text){
    return `<p>${text}</p>`;
}

function EditPostListener(clickedPost) {  
   	let textareaId = "edit_post_textarea";
    let textDiv = $(".post_body");
    let text = $(textDiv).find("p").text();

    $("#edit_post_button").click(function(){
        $(textDiv).html(getEditTextarea(text, textareaId));
        let textarea = $("#" + textareaId);
        
        $(textarea).keypress(function(e){
            let key = e.which;

            if (key == 13){ //enter key
                let newText = $(textarea).val();
				if(!newText && clickedPost.typeOfPost=="POST"){
					alert("Post must have text!");
					return;
				}
				$.post({
					url: '../rest/detailedView/editPost',
					data: postToJSON(clickedPost),
					contentType: 'application/json',
				});
                $(textDiv).html(getP(newText));
            }
        })
    });
}

function DeletePostListener(clickedPost){
	$("#delete_post_button").click(function(){
		if (!window.confirm('Are you sure you want to delete?'))
		{ return; }
		$.post({
			url: '../rest/detailedView/deletePost',
			data: postToJSON(clickedPost),
			contentType: 'application/json',
			success: function(post){
				alert("Successfully deleted post");
				redirectAfterDelete(post);
			}
		});
	});
}

function redirectAfterDelete(post){
	if(post.typeOfPost == "PICTURE"){
		window.location.replace("../front/gallery.html?"+post.username);	
	}
	else if(post.typeOfPost == "POST"){
		window.location.replace("../front/profile.html?"+post.username);
	}
}

function DeletePostAsAdminListener(clickedPost) {  
	let textareaId = "delete_post_reason_textarea";
	let textDiv = $(".post_body");

	$("#delete_post_button").click(function(){
		$(textDiv).html(getReasonTextarea(textareaId));
		let textarea = $("#" + textareaId);

		$(textarea).keypress(function(e){
			let key = e.which;

			if (key == 13){ //enter key
				let reasonText = $(textarea).val();

				if(!reasonText){
					alert("You must say a reason!");
					return;
				}
				if (!window.confirm('Are you sure you want to delete?'))
				{ return; }
				
				removePost(clickedPost, reasonText);
			}
		})
	});
}

function removePost(clickedPost, reasonText){
	$.post({
			url: '../rest/detailedView/deletePost/' + reasonText,
			data: postToJSON(clickedPost),
			contentType: 'application/json',
			success: function(post){
				alert("Successfully deleted post");
				redirectAfterDelete(post);
			}
		});
}

function SetAsProfilePicture(clickedPost){
	$("#set_as_profile_picture_button").click(function(){
		if (!window.confirm('Change your profile picture?'))
		{ return; }
		$.post({
			url: '../rest/detailedView/setAsProfilePic',
			data: postToJSON(clickedPost),
			contentType: 'application/json',
			success: function(user){
				//loggedUser = user;
				$("#user_profile_photo").attr("src", "../rest/ImageService/getImg/"+ user.profilePicture);
				$('[name=logged_user_photo]').attr("src", "../rest/ImageService/getImg/"+user.profilePicture);
				$("#navProfilePic").attr("src", "../rest/ImageService/getImg/"+user.profilePicture);
				reloadLoggedUser(user.username);
			}
		});
	});
	
}


function setPostDetails(clickedPost){
	setGeneralDetails(clickedPost);
	setLoggedUserDetails(clickedPost, loadLoggedUser());
}

function setGeneralDetails(clickedPost){
	$.get({
		url: '../rest/UserService/user/' + clickedPost.username,
		contentType: 'application/json',
		success: function(userWhoPosted){
			$("#user_profile_photo").attr("src", "../rest/ImageService/getImg/" + userWhoPosted.profilePicture)
			$("#user_fullname").text(userWhoPosted.firstName + " " + userWhoPosted.lastName);
			$("#post_timestamp").text(clickedPost.date);
			$("#post_text").text(clickedPost.text);
			$("#comments_num").text(clickedPost.comments.length);
			
			  if (clickedPost.photo !== ""){
				$(".post_picture").append('<img id="post_image" src="" alt="post picture">');
				$("#post_image").attr("src", "../rest/ImageService/getImg/" + clickedPost.photo);
			} 
		}
	});

	
}

function setLoggedUserDetails(clickedPost, loggedUser){
	if (loadLoggedUser()){
		if(loggedUser.role=="ADMIN"){
			$( ".edit_post_part" ).append('<i class="fas fa-trash-alt" id="delete_post_button"></i>');
			DeletePostAsAdminListener(clickedPost);
		}
		else if(clickedPost.username == loggedUser.username){
			$( ".edit_post_part" ).append('<i class="fas fa-edit" id="edit_post_button"></i>');
			$( ".edit_post_part" ).append('<i class="fas fa-trash-alt" id="delete_post_button"></i>');
			EditPostListener(clickedPost);
			DeletePostListener(clickedPost);
			
			if(post.typeOfPost == "PICTURE"){
				$( ".edit_post_part" ).append(' <i class="fas fa-user-circle" id="set_as_profile_picture_button"></i>');
				SetAsProfilePicture(clickedPost);
			}
		}
	}
}


$(document).ready(function() {
	
	getUser(function(user){
		// guest can not write comments
		if (!loadLoggedUser()){
			$(".write_new_comment").remove();
		}
		
		if (isOpenProfile(user)){
			getPost(function(clickedPost) {
				post = clickedPost;
				setPostDetails(clickedPost);
			});
		}else{
			$(".user_post").remove(); write_new_comment
			$(".posts").append(`<h3 style="color: var(--text);">User is private</h3>`);
		}
	});
	
	
});

function getPost(setPost){
	$.get({
		url: '../rest/detailedView/load',
		contentType: 'application/json',
		success: setPost
	});
}

function getDetViewUsername(){
    let parts = window.location.href.split('?');

    if (parts.length > 1){
        return parts[1];
    } else{
        return loadLoggedUser().username;
    }
}

function getUser(setUser){
	let username = getDetViewUsername();

	$.get({
		url: '../rest/UserService/user/' + username,
		contentType: 'application/json',
		success: setUser
	});
}

let loggedUser;
let post;



function postToJSON(clickedPost){
	return JSON.stringify({username: clickedPost.username,
								  postID: clickedPost.postID,
								  photo: clickedPost.photo,
								  text: clickedPost.text,
								  comments: clickedPost.comments,
								  date: clickedPost.date,
								  isDeleted: clickedPost.isDeleted,
								  typeOfPost: clickedPost.typeOfPost,
								  })	
}
/*function loadLikes(){
    let likes = $(".likes");
    for (let i = 0; i < 50; i++){
        htmlLike = getHtmlLike("profile");
        likes.append(htmlLike);
    }
}*/

/*function getHtmlLike(profile){
    let fullname = "Name Lastname";
    let imagePath = "images/other_profile_picture.jpg";
    return `
        <div class="like_user row post m-1 pb-1 pt-1">
            <div class="row like_user_info d-flex align-items-center">
                <div class="col-sm-2">
                    <img src="${imagePath}" alt="user profile photo" class="rounded-circle" id="like_user_profile_photo">
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                    <p id="like_user_fullname">${fullname}</p>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    `
}*/
