function loadAllFriends(friendsList, isMutual){
	let friends = $(".users");
	
    for (let i = 0; i < friendsList.length; i++){
		loadFriend(friendsList[i], isMutual);
    }

	friends.scrollTop(friends.prop("scrollHeight"));
}


function loadFriend(username, isMutual){
	 $.get({
        url: '../rest/UserService/user/' + username,
        contentType: 'application/json',
        success: function(user) {
			setHTML(user, isMutual);
        }
    });  
}

function setHTML(user, isMutual){
	$(".users").append(getHtmlMessage(user, isMutual));
	if(isMutual){
		addUnnectListener(user, "#unnect_", "unfollow");	
	}
	else{
		addUnnectListener(user, "#onect_", "follow");	
	}
	
    
	$(".users").scrollTop($(".users").prop("scrollHeight"));
}

function addUnnectListener(user, elemName, operation){
    $(elemName+user.username).click(function(){
        	$.post({
			url: '../rest/RequestService/'+operation+'/' + user.username,
			contentType: 'application/json',
			success: function(){ 
				$("#user_div_"+user.username).remove();
			 	location.reload();
			},
			error: function(message){
				alert(message.responseText);	
			}
    	});
    })
}

function getProfileUsername(){
    let parts = window.location.href.split('?');

    if (parts.length > 1){
        return parts[1];
    } else{
        return loadLoggedUser().username;
    }
}

function filterFriends(loggedUser, username){
	if(username==loggedUser.username){
		loadAllFriends(loggedUser.friends, true);
		$("#friend_title").text("My friends");
	}
	else{
		$("#friend_title").text("Mutual friends with "+username);
		
		getMutualFriends(username, function(mutualFriends) {
    		loadAllFriends(mutualFriends, true);			
		});
		/*getUser(username, function(otherUser) {				
			var otherFriends = otherUser.friends.filter(function(friendUsername) { return mutualFriends.inculdes(friendUsername) });
			$(".users").append(hr());
			loadAllFriends(otherFriends, false);
		});*/
	}
}

$(document).ready(function() {
    let username = getProfileUsername();
	
	getUser(loadLoggedUser().username, function(loggedUser) {
		filterFriends(loggedUser, username);
    })
	 
});
function getMutualFriends(username, callback){
	 $.get({
        url: '../rest/RequestService/mutuals/' + username,
        contentType: 'application/json',
        success: callback
    });
}
function getUser(username, callback){
	$.get({
        url: '../rest/UserService/user/' + username,
        contentType: 'application/json',
        success: callback
    });
}


function getHtmlMessage(otherUser, isMutual){
   var firstPart= `
          <div class="user row post mt-4 pt-2 pb-2" id="user_div_${otherUser.username}">
                    <div class="row user_info d-flex align-items-center">
                        <div class="col-sm-2">
                            <img src="../rest/ImageService/getImg/${otherUser.profilePicture}" alt="user profile photo" class="rounded-circle" id="user_profile_photo">
                        </div>
                        <div class="col-sm-4 ">
							<a href="/WebProjekat/front/profile.html?${otherUser.username}" style="color: black; text-decoration: none">
	                			<p id="user_fullname">${otherUser.firstName} ${otherUser.lastName}</p>
							</a>
                        </div>
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-3" align="right">
							 <a href="/WebProjekat/front/dm.html?${otherUser.username}"><i class="fas fa-comments send_message"></i></a>
						</div>`;
	
		var secondPart = `
						<div class="col-sm-auto">
							<button id="unnect_${otherUser.username}"class="btn btn-warning pull-right ignore_button">Un'nect</button>
                        </div>
                    </div>
                </div>
    	`;	
	if(!isMutual){
		secondPart = `
						<div class="col-sm-auto">
							<button id="onect_${otherUser.username}"class="btn btn-warning pull-right accept_button">O'nect</button>
                        </div>
                    </div>
                </div>
    	`;		
	}
	
	return firstPart + secondPart;
}

function hr(){
	return `<div class="mt-4 pt-2 pb-2">
		<hr>
	 </div>`;
}
