function getHtmlPost(user, post){
    let profilePicturePath = user.profilePicture;
    let fullName = user.firstName + " " + user.lastName;
    let imagePath = post.photo;
    let timestamp = post.date; 
    let text = post.text;
	
    let imageAdressParts = imagePath.split("\\");
    let imgId = imageAdressParts[imageAdressParts.length - 1].split(".")[0];
	
    let numOfComments = 0;
	if (post.comments){
		numOfComments = post.comments.length;
	}

    htmlPost =  `
        <div class="user_post row post mt-4 pt-2">
            <div class="row post_info d-flex align-items-center">
                <div class="col-sm-2">
                    <img src="../rest/ImageService/getImg/${profilePicturePath}" alt="user profile photo" class="rounded-circle" id="user_profile_photo">
                </div>
                <div class="col-sm-4 ">
                    <p id="user_fullname">${fullName}</p>
                </div>
                <div class="col-sm-2">
                </div>
                <div class="col-sm-auto" align="right">
                    <p id="post_timestamp">${timestamp}</p>
                </div>
            </div> 
            <div class="row post_body pt-3">
                <p>${text}</p>
            </div>
            <div class="row post_picture pb-3">
            `;

    if (imgId !== ""){
        htmlPost += ` <img src="../rest/ImageService/getImg/${imgId}" alt="post picture">`
    }    

    htmlPost +=
            `
            </div>
            <div class="row post_footer " >
                <p class="col-sm-10"></p>
                <i class="fas fa-comments comment_post col-sm-1" id="comment_icon_${post.postID}" align="right"></i>
                <p class="col-sm-1 num_of_comments" align="left">${numOfComments}</p>
            </div>
        </div>
    `;

    return htmlPost;
}

function loadPosts(loadedPosts){
    if (loadedPosts.length != 0){
        for (post of loadedPosts){
            getUserAndLoadPost(post)
        }
    } else{
        $(".posts").append(`<h3 style="color: var(--text);">User hasn't posted anything yet.</h3>`);
    }
}

function loadUserIsPrivateH3(){
    let posts = $(".posts");
    htmlPost = `<h3 style="color: var(--text);">User is private</h3>`;
    posts.prepend(htmlPost);
}

function getUserAndLoadPost(post){
    let username = post.username;

    $.get({
        url: '../rest/UserService/user/' + username,
        contentType: 'application/json',
        success: function(user) {
            loadPost(user, post);
        }
    });
}

function loadPost(user, post){
    let posts = $(".posts"); 
    htmlPost = getHtmlPost(user, post);	
    posts.prepend(htmlPost);
	$("#comment_icon_"+post.postID).click( function(){
		 saveChosenPost(post);
	});
}

// function setImg(imageElemId, imgServerId){
//     $("#" + imageElemId).attr("src","business/getImg?addr="+imgServerId+"");
// }

function getTypeOfNewPost(){
    let postAsPhotoCB = $("#postAsPhotoCB"); 
    let typeOfPost = "POST";

    if ($(postAsPhotoCB).is(":checked"))
    {
        typeOfPost = "PICTURE";
    }

    return typeOfPost;
}

function getNewPostImage(){
    let image = new FormData();
    image.append('file', $("input[name=file]")[0].files[0]);

    return image;
}

function isFileUploaded(){
    return $("input[name=file]")[0].files.length !== 0;
}

function validateByTypeOfPost(typeOfPost, text){
    if (typeOfPost === "POST" && text == ""){
        alert("To post a new Post you need to write something.")
        return false;
    }
    else if (typeOfPost === "PICTURE" && !isFileUploaded()){
        alert("To post a new Photo you need to upload an image.")
        return false;
    }

    return true;
}

function sendPostToServer(user, postJson){
    $.ajax({
        url:"/WebProjekat/rest/PostService/newPost",
        type:"POST",
        data:JSON.stringify(postJson),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(post){
            if (postJson.typeOfPost === "POST"){
                loadPost(user, post);
            }
            
            clearNewPostInputs();

			location.reload();
        }
    });
}

function sendPicturePostToServer(user, postJson, image){
    $.ajax({
        url:"../rest/PostService/uploadImage",
        type:"POST",
        data:image,
        cache: false,
        contentType: false,
        processData: false,
        success: function(imagePath){
            postJson.photo = imagePath;
            sendPostToServer(user, postJson);
        }
    });
}

function addNewPostListener(user){
    let textarea = $("#new_post_textarea");
    let postButton = $("#post_button");


    $(postButton).click(function(){
        let typeOfPost = getTypeOfNewPost();
        let inputText = $(textarea).val().trim();
        let image = getNewPostImage();

        if (validateByTypeOfPost(typeOfPost, inputText)){
            let postJson = createJsonPost(user.username, typeOfPost, inputText, "");

            if (isFileUploaded()){
                sendPicturePostToServer(user, postJson, image);
            }else{
                sendPostToServer(user, postJson);
            }
        }

    })
}

function clearNewPostInputs(){
    let textarea = $("#new_post_textarea");
    let postAsPhotoCB = $("#postAsPhotoCB");
    textarea.val('').blur();
    $("input[name=file]")[0].value = '';
    postAsPhotoCB.prop( "checked", false )


}

function createJsonPost(username, typeOfPost, inputText, imagePath){
    return {
        username : username,
        typeOfPost : typeOfPost,
        photo : imagePath,
        text : inputText
    }
}


function saveChosenPost(post){
	$.post({
		url: '../rest/detailedView/save',
		data: JSON.stringify({username: post.username,
							  postID: post.postID,
							  likesNum: post.likesNum,	
							  photo: post.photo, 
								text: post.text,
								comments: post.comments,
								typeOfPost: post.typeOfPost,
								isDeleted: post.isDeleted,
								date: post.date}),
		contentType: 'application/json',
		success: function() {
			window.location.replace("../front/detailed_post.html?" + post.username);
		},
		error: function(message) {
			alert(message.responseText);
		}
	});
}
