function loadAllChats(users){
	let dms = $(".users");
	
    for (let i = 0; i < users.length; i++){
		loadUser(users[i]);
    }

	dms.scrollTop(dms.prop("scrollHeight"));
}


function loadUser(user){
    let dms = $(".users");
	
    htmlMessage = getHtmlMessage(user);
	dms.append(htmlMessage);
	loadAllMessagesListener(user);

    if (user.role == "ADMIN"){
        $("#chat_with_" + user.username).removeClass("user");
        $("#chat_with_" + user.username).addClass("admin_message_background_color");
    }

    dms.scrollTop(dms.prop("scrollHeight"));
}

function loadAllMessagesListener(user){
    $("#view_Dms_With_"+user.username).click(function(){
        $(location).attr('href',"/WebProjekat/front/dm.html?" + user.username);
    })
}

$(document).ready(function() {
	
	loggedUser = loadLoggedUser();
		
	getDMs(function(users) {
    	loadAllChats(users);
	});
});

function getDMs(callback){
	    
	$.get({
		url: '../rest/DMService/loadUsersFromMyDMs',
		contentType: 'application/json',
		success: callback
	});
}

let loggedUser;

function getHtmlMessage(otherUser){

	var imagePath = otherUser.profilePicture;
   return`
          <div class="user row post mt-4 pt-2 pb-2" id="chat_with_${otherUser.username}">
                    <div class="row user_info d-flex align-items-center">
                        <div class="col-sm-2">
                            <img src="../rest/ImageService/getImg/${imagePath}" alt="user profile photo" class="rounded-circle" id="user_profile_photo">
                        </div>
                        <div class="col-sm-4 ">
							<a href="/WebProjekat/front/profile.html?${otherUser.username}" style="color: black; text-decoration: none">
	                			<p id="user_fullname">${otherUser.firstName} ${otherUser.lastName}</p>
							</a>
                        </div>
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-auto" align="right">
							 <button id="view_Dms_With_${otherUser.username}"class="btn btn-warning pull-right accept_button">View all</button>
						</div>
                    </div>
                </div>
    `;	
}
