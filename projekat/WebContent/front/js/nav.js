function getNavHtml(){
    let user = loadLoggedUser();

    return `
    <a class="navbar-brand" href="/WebProjekat/front/feed.html">
                <img src="images/orange_cropped_60.png"  class="brand" alt="orangenect logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav">
                <span><i class="fas fa-bars burger-menu"></i></span>
            </button>
            <form class="form-inline">
                <input class="form-control mr-sm-2 searchField" type="search" placeholder="Search" aria-label="Search">
            </form>
            <button class="btn search_btn" id="searchButtonNav"><i class="fas fa-search"></i></button>
            <div class="collapse navbar-collapse" id="navbar-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a id="homeButton" class="nav-link navLink" href="/WebProjekat/front/feed.html" target="_top"><i class="fas fa-home"></i> HOME</a>
                    </li>
                    <li class="nav-item">
                        <a id="inboxButton" class="nav-link navLink" href="../front/all_chats.html" target="_top"><i class="far fa-comments inbox_icon"></i> Inbox</a>
                    </li>
                    <li class="nav-item">
                        <a id="friendsButton" class="nav-link navLink" href="../front/friend_requests.html" target="_top"><i class="fas fa-user-friends friends_icon"></i> O'nect Requests</a>
                    </li>
                    <li class="nav-item">
                        <a id="notificationButton" class="nav-link navLink" href="../front/settings.html" target="_top"><i class="fas fa-cogs"></i> Settings</a>
                    </li>
                    <li class="nav-item">
                        <a id="profileButton" class="nav-link navLink" href="/WebProjekat/front/profile.html" target="_top"><img src="../rest/ImageService/getImg/${user.profilePicture}" alt="profile_picture" class="profile_picture" id="navProfilePic"> Profile</a>
                    </li>
                    <li class="nav-item">
                        <a id="logoutButton" class="nav-link navLink" href="" target="_top"><i class="fas fa-chevron-circle-right"></i> Logout</a>
                    </li>
                </ul>
            </div>    
    `;
}

function getGuestNavHtml(){
    return `
        <a class="navbar-brand" href="/WebProjekat/front/login.html">
            <img src="images/orange_cropped_60.png"  class="brand" alt="orangenect logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav">
            <span><i class="fas fa-bars burger-menu"></i></span>
        </button>
        <form class="form-inline">
            <input class="form-control mr-sm-2 searchField" type="search" placeholder="Search" aria-label="Search">
        </form>
        <button class="btn search_btn" id="searchButtonNav" ><i class="fas fa-search"></i></button>
        <div class="collapse navbar-collapse" id="navbar-nav">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link navLink" href="/WebProjekat/front/registration.html" target="_top">Join now</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link navLink" href="/WebProjekat/front/login.html" target="_top">Sign in</a>
                </li>
            </ul>
        </div>
    `
}

function addLogoutListener(){
    $("#logoutButton").click( function(){
        $.post({
            url: '../rest/LoginService/logout',
            contentType: 'application/json',
            success: function() {
                removeLoggedUser();
                $(location).attr('href',"/WebProjekat/front/login.html");
            }
        });
    });
}

function addSearchButtonListener(){
    $("#searchButtonNav").click( function(){
        $(location).attr('href',"/WebProjekat/front/search.html");
    });
}

function loadNavbar(){
    let container = $(".navbar_container");
    let htmlNavbar = getNavHtml();
    container.append(htmlNavbar);
    addLogoutListener();
}

function loadGuestNavbar(){
    let container = $(".navbar_container");
    let htmlNavbar = getGuestNavHtml();
    container.append(htmlNavbar);
}

$(document).ready(function() {
    if (localStorage.getItem("user")){
		loadNavbar();
	} else {
        loadGuestNavbar();
    }

    addSearchButtonListener();
});