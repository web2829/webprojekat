function loadAllMessages(allMessages){
	let messages = $(".messages");
	
    for (let i = 0; i < allMessages.length; i++){
		loadNewMessage(allMessages[i]);
    }

	messages.scrollTop(messages.prop("scrollHeight"));
}


function loadNewMessage(dm){
    let messages = $(".messages");

    htmlMessage = getHtmlMessage(dm);
	messages.append(htmlMessage);

    messages.scrollTop(messages.prop("scrollHeight"));
}

function addSendMessageListener(socket){
    let textarea = $("textarea");
    $(textarea).keypress(function(e){
        let key = e.which;
        let inputMessage = $(textarea).val();

        if (key == 13 && inputMessage.trim().length !== 0){ //enter key
            sendToServer(socket, inputMessage);
			$(textarea).val("");
		}
    });
}

function sendToServer(socket, inputMessage){
	let dm = {senderUsername: loggedUser.username,
			recieverUsername: otherUser.username,
			date: getDateNow(),
			message: inputMessage}
	socket.send(JSON.stringify(dm));
	loadNewMessage(dm);
}


function sendMessage(message) {
	socket.send(message);
	addMessageToWindow("Sent Message: " + message);
}

$(document).ready(function() {
	loggedUser = loadLoggedUser();
	const socket = new WebSocket("ws://localhost:8088/WebProjekat/dmWebsocket/" + loggedUser.username);

    socket.onopen = function (event) {
        console.log("Connected");
    };

    socket.onmessage = function (event) {
		let dm = JSON.parse(event.data);

		if (dm.senderUsername == otherUser.username){
			loadNewMessage(dm);
		}
    };
		
	getOtherUser(function(user) {
		otherUser = user;
		
		getDMs(function(dms) {
	    	loadAllMessages(dms);
			addSendMessageListener(socket);
			
			if (otherUser.role == "ADMIN"){
				$(".new_message").remove();
			}
		});
	});

	
});


function getOtherUsername(){
    let parts = window.location.href.split('?');

    if (parts.length > 1){
        return parts[1];
    } 
}

function getOtherUser(callback){
	otherUserUsername = getOtherUsername();
	$.get({
		url: '../rest/UserService/user/' + otherUserUsername,
		contentType: 'application/json',
		success: callback
	});
	
}

function getDMs(callback){
	$.post({
		url: '../rest/DMService/loadDMs',
		data: JSON.stringify({username: otherUser.username}),
		contentType: 'application/json',
		success: callback
	});
}

let loggedUser;
let otherUser;

function getHtmlMessage(dm){
    let timestamp = dm.date + " " ;

	var imagePath;
	if(dm.senderUsername == loggedUser.username){
		imagePath = loggedUser.profilePicture;
	}
	else if(dm.senderUsername == otherUser.username){
		imagePath = otherUser.profilePicture;
	}

    return `
        <div class="message row post mt-4 pt-2">
            <div class="row message_info d-flex align-items-center">
                <div class="col-sm-2">
                    <img src="../rest/ImageService/getImg/${imagePath}" alt="user profile photo" class="rounded-circle" id="user_profile_photo">
                </div>
                <div class="col-sm-4 ">
                </div>
                <div class="col-sm-2">
                </div>
                <div class="col-sm-auto" align="right">
                    <p id="post_timestamp">${timestamp}</p>
                </div>
            </div>
            <div class="row message_body pt-3">
                <p>${dm.message}</p>
            </div>
        </div>
    `
}
