function storeLoggedUser(user){
    let stringifyUser = JSON.stringify(user);
    localStorage.setItem("user", stringifyUser);
}

function loadLoggedUser(){
    let stringifyUser = localStorage.getItem("user");

    return JSON.parse(stringifyUser)
}

function removeLoggedUser(){
    localStorage.removeItem("user");
}

function reloadLoggedUser(username){
    $.get({
        url: '../rest/UserService/user/' + username,
        contentType: 'application/json',
        success: function(user) {
            removeLoggedUser();
            storeLoggedUser(user);
        }
    });
}