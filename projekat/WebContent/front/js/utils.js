function validateRegex(regex, input){
    return regex.test(input)
}

function validateDate(date){
    regex = /^([0-2][0-9]|(3)[0-1])(.)(((0)[0-9])|((1)[0-2]))(.)\d{4}$/;

    return validateRegex(regex, date);
}

function validateEmail(email){
    regex = /\S+@\S+\.\S+/;

    return validateRegex(regex, email);
}

function containsDigits(input){
    regex = /\d/;  

    return validateRegex(regex, input);
}

function isAllLetters(input){
    regex = /^[a-zA-Z]+$/;

    return validateRegex(regex, input);
}

function isOpenProfile(user){
    let isOpen;
    let isPublic = !user.privateProfile;

    if (loadLoggedUser()){
        let isSelfProfile = loadLoggedUser().username == user.username;
        let isFriend = loadLoggedUser().friends.indexOf(user.username) != -1;
        let isAdminLogged = loadLoggedUser().role == "ADMIN";
        isOpen = isSelfProfile || isPublic || isFriend || isAdminLogged;
    } else {
        // guest can see only public profiles
        isOpen = isPublic;
    }

    return isOpen;
    }

function getDateNow(){
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); 
	var yyyy = today.getFullYear();
	
	today = mm + '/' + dd + '/' + yyyy;
	return today;

}