function getHtmlImage(image, id){
    //let imagePath = "images/orange_from_inside_cropped_1200.jpg";
	let imagePath = image.photo; 
    return `
        <div class="col-sm-4 mt-2"> 
            <div class="image p-1 ">
                <img class="img-fluid" src="../rest/ImageService/getImg/${imagePath}" id="image_${id}"alt="post picture">
            </div>
        </div>
    `
}

function loadImages(loadedImages){
    let images = $(".images");

	if (loadedImages.length != 0){
        for (let i = 0; i < loadedImages.length; i++){
			htmlImage = getHtmlImage(loadedImages[i], i);
			images.append(htmlImage);
			let imageID = "#image_" + i;
			$(imageID).click( function(){
				 saveChosenImage(loadedImages[i]);
			});
		}
    } else{
        $(".images").append(`<h3 style="color: var(--text);">User hasn't posted any photo yet.</h3>`);
    }

    
}

function saveChosenImage(image){
	$.post({
		url: '../rest/detailedView/save',
		data: JSON.stringify({username: image.username,
							  postID: image.postID,
							  likesNum: image.likesNum,	
							  photo: image.photo, 
								text: image.text,
								comments: image.comments,
								typeOfPost: image.typeOfPost,
								isDeleted: image.isDeleted,
								date: image.date}),
		contentType: 'application/json',
		success: function() {
			window.location.replace("../front/detailed_post.html?" + image.username);
		},
		error: function(message) {
			alert(message.responseText);
		}
	});
}

function getGalleryUsername(){
    let parts = window.location.href.split('?');

    if (parts.length > 1){
        return parts[1];
    } else{
        return loadLoggedUser().username;
    }
}

$(document).ready(function() {
	let username = getGalleryUsername();
	$.get({
		url: '../rest/UserService/user/' + username,
		contentType: 'application/json',
		success: function(user) {
			
			if (isOpenProfile(user)){
				loadImages(user.myImages);
			}else{
				$(".images").append(`<h3 style="color: var(--text);">User is private</h3>`);
			}
			
		}
	});
});