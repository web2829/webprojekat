function setFullname(fullname){
    let fullnameP = $("#user_profile_fullname");
    $(fullnameP).text(fullname);
}

function setProfilePicture(user){
    let imgId = user.profilePicture;
    let profilePhoto = $("#person");
    src = "../rest/ImageService/getImg/" + imgId;
    $(profilePhoto).attr("src", src);
    // add click listener to redirect to detailed view
}

function setStats(user){
    let postsNum = user.myPosts.length;
    let friendsNum = user.friends.length;
    let photosNum = user.myImages.length;
    $("#posts_num").text(postsNum);
    $("#friends_num").text(friendsNum);
    $("#photos_num").text(photosNum);
}

function setConnectButton(user){
    if (loadLoggedUser()){
        loggedUserUsername = loadLoggedUser().username;
        $.get({
            url: '../rest/UserService/user/' + loggedUserUsername,
            contentType: 'application/json',
            success: function(loggedUser){
                if(loggedUser.username===user.username || user.role == "ADMIN"){
                    $("#follow").remove();
                }
                
                let areFriends = loggedUser.friends.includes(user.username);
                isRequestSent(areFriends, user);
            }
        });
    } else{
        $("#follow").remove();
    }
	
}

function isRequestSent(areFriends, user){
	$.get({
			url: '../rest/RequestService/isRequestSent/' + user.username,
			contentType: 'application/json',
			success: function(requestSent){ 
				setOption(areFriends, requestSent, user);
			}
    	});
}
function setOption(areFriends, requestSent, user){
	if (areFriends){
		unfollowOption(user);
    }
	else{
		if(requestSent){
			waitingResponse();
		}
		else{
			followOption(user);
		}
    }
}

function followOption(user){
	$("#follow").text("O'NECT");
	$("#follow").unbind();
    addFollowListener("follow", user);
}

function waitingResponse(){
	$("#follow").text("WAITING");
	$("#follow").unbind();
	$("#follow").attr('disabled', true);
}

function unfollowOption(user){
	$("#follow").text("UN'NECT");
	$("#follow").unbind();
    addFollowListener("unfollow", user);
}

function changeOption(operation, user){
	if(operation==="follow"){
		waitingResponse();
	}
	else if(operation==="unfollow"){
		followOption(user);
	}
	
}

function addFollowListener(operation, user){
	
	 $("#follow").click(function(){
        
		$.post({
			url: '../rest/RequestService/' + operation + '/' + user.username,
			contentType: 'application/json',
			success: function(){ 
				changeOption(operation, user);
			},
			error: function(message){
				alert(message.responseText);	
			}
    	});
	});
}

function setGalleryButton(user){
    if (isOpenProfile(user)){
        $("#photos_label").click(function(){
            $(location).attr('href',"/WebProjekat/front/gallery.html?" + user.username);
        })
    }
}

function addSettingsListener(username){
	$("#settingsUserButton").click(function(){
        $(location).attr('href',"/WebProjekat/front/settings.html?" + username);
    })
}
function openChatWithUserButtonListener(username){
	$("#openChatWithUserButton").click(function(){
        $(location).attr('href',"/WebProjekat/front/dm.html?" + username);
    })
}
function addBlockListener(username, operation){
	$("#blockUserButton").click(function()
		{if (!window.confirm('Are you sure you want to '+ operation +' this user?'))
		{ return; }
        $.post({
			url: '../rest/LoginService/' +operation+ '/' + username,
			contentType: 'application/json',
			success: function(){
				alert("Successfully " + operation +"!"); 
				setOtherOption(operation, username);
			},
			error: function(message){
				alert(message.responseText);	
			}
    	});
    })
}

function setOtherOption(operation, username){
	$("#blockUserButton").empty();
	$("#blockUserButton").unbind();
	if(operation=="block"){
		$("#blockUserButton").append('<i class="far fa-check-circle unblock_user"></i>');
		addBlockListener(username, "unblock");	
	}
	else{
		$("#blockUserButton").append('<i class="fas fa-ban block_user"></i>');	
		addBlockListener(username, "block");
	}
	
	
}


function setChatSettingsBlockButtons(user){
    //TODO add listeners 
    
    if (loadLoggedUser()){
        // user's profile
        if (user.username == loadLoggedUser().username){
            // only settings is available
            $("#openChatWithUserButton").remove();
            $("#blockUserButton").remove();
			addSettingsListener(loadLoggedUser().username);
			
        // other user's profile
        } 
		else{
			// can't edit other user's settings
            $("#settingsUserButton").remove();
			
			// other user is admin
			if(user.role == "ADMIN"){
				$("#openChatWithUserButton").remove();
			}
			// if logged user isn't an admin, can't block other users
            if (loadLoggedUser().role != "ADMIN"){
                $("#blockUserButton").remove();
            }
			else{
				// if other user already blocked, can not be blocked again
				if(user.blocked){
					//<i class="far fa-check-circle"></i>
					//$(".block_user").removeClass('fas fa-ban unblock_user').addClass('fas fa-check-circle');
					//<i class="fas fa-ban block_user"></i>
					$("#blockUserButton").empty();
					$("#blockUserButton").append('<i class="far fa-check-circle unblock_user"></i>');
					addBlockListener(user.username, "unblock");
				}
				else{
					addBlockListener(user.username, "block");	
				}
			}
			openChatWithUserButtonListener(user.username);
        }
    } 
	else{
        // guest can not interact with user in anyway
        $("#openChatWithUserButton").remove();
        $("#blockUserButton").remove();
        $("#settingsUserButton").remove();
    }
}

function setFriendsButton(username){
    if (loadLoggedUser()){
        $("#friends_label").click(function(){
            $(location).attr('href',"/WebProjekat/front/friends.html?" + username);
        })
    }
}

function setLeftPartOfProfile(user){
    setFullname(user.firstName + " " + user.lastName);
    setProfilePicture(user);
    setStats(user);
    setConnectButton(user);
    setGalleryButton(user);
    setChatSettingsBlockButtons(user);
	setFriendsButton(user.username);
}

function getProfileUsername(){
    let parts = window.location.href.split('?');

    if (parts.length > 1){
        return parts[1];
    } else{
        return loadLoggedUser().username;
    }
}

$(document).ready(function() {
    let username = getProfileUsername();

    $.get({
        url: '../rest/UserService/user/' + username,
        contentType: 'application/json',
        success: function(user) {
            setLeftPartOfProfile(user);

            if (isOpenProfile(user)){
                loadPosts(user.myPosts);
            } else {
                loadUserIsPrivateH3();
            }
            
            if (loadLoggedUser() && user.username == loadLoggedUser().username){
                addNewPostListener(user);
            }else{
                // can't post to other user's profile
                $(".new_post").remove();
            }
        }
    });

    // listener za chat, block, settings
    // listener za friend list, na O'nects
});