function getEditTextarea(currentText, id){
    return `<textarea id="${id}" class="form-control input-lg p-text-area">${currentText}</textarea>`;
}

function getP(text){
    return `<p>${text}</p>`;
}

function EditCommentListener(comment) { 
	let textareaId = "edit_comment_textarea_" + comment.commentID;
    let editButton = $("#edit_comment_button_" + comment.commentID);
    let textDiv = $("#comment_text_" + comment.commentID);
    let text = $(textDiv).find("p").text();

    $(editButton).click(function(){
        $(textDiv).html(getEditTextarea(text, textareaId));
        let textarea = $("#" + textareaId);
        $(textarea).keypress(function(e){
            let key = e.which;

            if (key == 13){ //enter key
                let newText = $(textarea).val();
				$.post({
					url: '../rest/detailedView/editComment',
					data: JSON.stringify({commentID: comment.commentID,
										  postID: comment.postID,
										  userName: comment.userName,
										  text: newText,
										  date: comment.date,
										  dateOfChange: comment.dateOfChange,
										  isDeleted: comment.isDeleted
										  }),
					contentType: 'application/json',
					success: function(comment){
						$(textDiv).html(getP(newText));
						var datePName = "edited_comment_timestamp_" +comment.commentID;
						$('p[name='+datePName+']').text("edited: " + comment.dateOfChange);
					} 
				});
            }
        })
    });
}

function DeleteCommentListener(comment){
	$("#delete_comment_button_" + comment.commentID).click(function(){
        if (!window.confirm('Are you sure you want to delete?'))
		{ return; }
		$.post({
			url: '../rest/detailedView/deleteComment',
			data: JSON.stringify({commentID: comment.commentID,
								  postID: comment.postID,
								  userName: comment.userName,
								  text: comment.text,
								  date: comment.date,
								  dateOfChange: comment.dateOfChange,
								  isDeleted: comment.isDeleted
								  }),
			contentType: 'application/json',
			success: function(comment){
				$("#comment_div_"+comment.commentID).remove();
				//$("#comments_num").val(parseInt($("#comments_num").val())-1);
			}
		});
	});
}
function NewCommentListener(image){
    let textarea = $("#new_comment_textarea");
    let commentButton = $("#new_comment_button");

    $(commentButton).click(function(){
        let inputText = $(textarea).val().trim();

        if (inputText.length !== 0){ 
			$.post({
					url: '../rest/detailedView/addComment',
					data: JSON.stringify({postID: image.postID,
										  text: inputText}),
					contentType: 'application/json',
					success: function(comment) {
						loadComment(comment);
						$(textarea).val("");
						//$("#comments_num").text( $("#comments_num").val() + 1 );
					}
				});
		
        } else if (inputText.length === 0){
            $(textarea).val("");
        }
    })

}

function loadComments(allComments){
    for (let i = 0; i < allComments.length; i++){
       loadComment(allComments[i])
    }
}

function loadComment(comment){
	$.get({
		url: '../rest/UserService/getProfilePic/'+comment.userName,
		contentType: 'application/json',
		success: function(user){
			
			let editable = false;
			if(loadLoggedUser()){
				editable = comment.userName==loadLoggedUser().username;	
			}
		    $(".comments").append(getHtmlComment(user.profilePicture, comment, editable));
		    EditCommentListener(comment);
			DeleteCommentListener(comment);
		},
		error: function(message) {
				alert(message.responseText);
		}
	});
	
}

$(document).ready(function() {
	
	getPost(function(post) {		
		$.get({
			url: '../rest/UserService/user/' + post.username,
			contentType: 'application/json',
			success: function(user) {
				if (isOpenProfile(user)){
					loadComments(post.comments);
	    			NewCommentListener(post);
				}else{
					$(".write_new_comment").remove();
					$(".comments").append(`<h3 style="color: var(--text);">User is private</h3>`);
				}
			}
		});
	});	
	
});

function getPost(setPost){
	$.get({
		url: '../rest/detailedView/load',
		contentType: 'application/json',
		success: setPost
	});
}

function getHtmlComment(profilePic, comment, editable){
	let html_before_change_date = `
        <div class="comment row post m-1 pt-2" id="comment_div_${comment.commentID}">
            <div class="row commenter_info d-flex align-items-center">
                <div class="col-sm-2">`;
	if(editable){
		html_before_change_date += `<img src="../rest/ImageService/getImg/${profilePic}" alt="user profile photo" class="rounded-circle" id="comment_user_profile_photo" name="logged_user_photo">`;
	}
	else{
		html_before_change_date += `<img src="../rest/ImageService/getImg/${profilePic}" alt="user profile photo" class="rounded-circle" id="comment_user_profile_photo">`;
	}
                    
  	html_before_change_date +=  `</div>
                <div class="col-sm-7 ">
						<a href="/WebProjekat/front/profile.html?${comment.userName}" style="color: black; text-decoration: none">
		                	<p id="comment_user_fullname">${comment.userName}</p>
						</a>
                    
                </div>
                <div class="col-sm-3 edit_comment_part"> `;
	if(editable){
		html_before_change_date += `<i class="fas fa-edit" id="edit_comment_button_${comment.commentID}"></i> 
									<i class="fas fa-trash-alt" id="delete_comment_button_${comment.commentID}"></i>`;
	}                    
     html_before_change_date +=`
                </div>
            </div>
            <div class="row comment_timestamps">
                <div class="col-sm-6"> `;
                   
				
	let html_after_change_date = `
                </div>
                <div class="col-sm-6" align="right">
                    <p id="comment_timestamp">${comment.date}</p>
                </div>
            </div>
            <div class="row comment_body pt-1" id="comment_text_${comment.commentID}">
                <p>${comment.text}</p>
            </div>
        </div>
    `

	if(comment.dateOfChange!="null"){
		return html_before_change_date + `<p id="edited_comment_timestamp" name="edited_comment_timestamp_${comment.commentID}"  >edited: ${comment.dateOfChange}</p>` + html_after_change_date;
	}
	return html_before_change_date + `<p id="edited_comment_timestamp" name="edited_comment_timestamp_${comment.commentID}" ></p>` + html_after_change_date;
}
