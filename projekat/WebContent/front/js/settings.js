function validateName(name){
    return validateFilledInput(name, isAllLetters, "nameErrorLabel");
}

function validateBirthdate(date){
    return validateFilledInput(date, validateDate, "birthdateErrorLabel");
}

function validateEmailInput(email){
    return validateFilledInput(email, validateEmail, "emailErrorLabel");
}

function validateFilledInput(input, validateFunc, errorLabelId){
    if (input !== ""){
        if (!validateFunc(input)){
            writeErrorLabel(errorLabelId);
            return false;
        }

        return true
    }
    return true;
}

function validatePassword(password, retypedPassword){
	if (password !== ""){
		if (retypedPassword !== ""){
			if (password === retypedPassword){
				return true;
			}
			else{
				writeErrorLabel(retypePasswordErrorLabel, "Needs to match password.");
				return false;
			}
		}
		else{
			writeErrorLabel(retypePasswordErrorLabel, "Retype password to change it.");
			return false;
		}
    }

    return true;
}

function writeErrorLabel(errorLabelId, message="Invalid input"){
	$('#' + errorLabelId).text(message);
	$('#' + errorLabelId).show().delay(2000).fadeOut();
}

function validateInputs(firstName, lastName, date, email, password, retypedPassword){
	valid = validateName(firstName);
	valid &= validateName(lastName);
	valid &= validateBirthdate(date);
	valid &= validateEmailInput(email);
	valid &= validatePassword(password, retypedPassword);
	return valid;
}

function reset(){
	$('#nameInput').val("");
	$('#lastnameInput').val("");
	$("#birthdateInput").val("");
	$('#emailInput').val("");
	$('#passwordnput').val("");
	$('#retypedPasswordInput').val("");
	
}

$(document).ready(function() {
	loggedUser = loadLoggedUser();
	setDetails(loggedUser);
	
	$('#settingsForm').submit(function() {

		let firstName = $('#nameInput').val().trim();
		let lastName = $('#lastnameInput').val().trim();
		let date = $('#birthdateInput').val().trim();
		let email = $('#emailInput').val().trim();
		let password = $('#passwordnput').val().trim();
		let retypedPassword = $('#retypedPasswordInput').val().trim();
		let isPrivate = $("#private").is(':checked');

		if (!validateInputs(firstName, lastName, date, email, password, retypedPassword)){
			return;
		}
		
		$.post({
			url: '../rest/RegistrationService/change',
			data: JSON.stringify({firstName: firstName,
								  lastName: lastName,
								  email: email,
								  password: password,
								  birthDate: date,
								  privateProfile: isPrivate	
								  }),
			contentType: 'application/json',
			success: function(){ 
				alert("Successfully updated your profile");
				reset();
			},
			error: function(){
				alert("Oops, something went wrong!");	
			}
		});
	});
});


function setDetails(user){
	$("#profile_pic_preview").attr("src", "../rest/ImageService/getImg/"+user.profilePicture);
	$("#username_span").text(user.username);
	$("#gallery_ref").attr("href", "/WebProjekat/front/gallery.html?"+user.username);
}