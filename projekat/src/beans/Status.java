package beans;

public enum Status {
	WAITING,
	ACCEPTED,
	CANCELED,
	REJECTED;
}
