package beans;

import java.util.ArrayList;
import java.util.Date;

import helpers.DateTimeManipulation;

public class Post {
	private String username;	// whose post it is?
	private int postID;
	private String photo; //
	private String text;
	private ArrayList<Comment> comments;
	private boolean isDeleted;
	private TypeOfPost typeOfPost;
	private String date;
	
	public Post() {
	}
	
	// for creating new post
	public Post(int postID, String username, String photo, String text, TypeOfPost typeOfPost) {
		super();
		this.username = username;
		this.postID = postID;
		this.photo = photo;
		this.text = text;
		this.typeOfPost = typeOfPost;
		// if POST --> text required, photo optional
		// if PICTURE --> photo required, text optional
		
		this.date = DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate());
		this.comments = new ArrayList<Comment>();
		this.isDeleted = false;
	}
	
	// for reading from file
	public Post(int postID, String username, String photo, String text, boolean isDeleted,  TypeOfPost typeOfPost, String date, ArrayList<Comment> comments) {
		super();
		this.username = username;
		this.postID = postID;
		this.photo = photo;
		this.text = text;
		this.typeOfPost = typeOfPost;
		this.comments = comments;
		this.isDeleted = isDeleted;
		this.date = date;
	}
	public String getUsername() {
		return this.username;
	}
	public int getPostID() {
		return postID;
	}
	public void setPostID(int postID) {
		this.postID = postID;
	}
	public String getPhoto() {
		return photo;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public ArrayList<Comment> getComments() {
		return comments;
	}
	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String toCSV() {
		//postID;userName;photo;text;likes;isDeleted;typeOfPost;date

		return postID + ";" + username + ";" + photo + ";" + text + ";" + isDeleted + ";" + typeOfPost + ";" + date;

	}

	public TypeOfPost getTypeOfPost() {
		return typeOfPost;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setTypeOfPost(TypeOfPost typeOfPost) {
		this.typeOfPost = typeOfPost;
	}
	
	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
		
	}
}
