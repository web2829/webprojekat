package beans;

import java.util.Date;
import java.util.Objects;

import helpers.DateTimeManipulation;

public class Comment {
	private int commentID;
	private int postID;
	private String userName;			//who commented
	private String text;
	private String date;
	private String dateOfChange; // if changed, date of the last change
	private boolean isDeleted;

	
	public Comment() {
		super();
	}
	
	// new comment
	public Comment(int postID, int commentID, String userName, String text) {
		this.commentID = commentID;
		this.postID = postID;
		this.userName = userName;
		this.text = text;
		
		this.date = DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate());
		this.dateOfChange = "null";
		this.isDeleted = false;
	}
	
	// reading from file
	public Comment(int postID, int commentID, String userName, String text, String date, String dateOfChange, boolean isDeleted) {
		super();
		this.commentID = commentID;
		this.postID = postID;
		this.userName = userName;
		this.date = date;
		this.dateOfChange = dateOfChange;
		this.isDeleted = isDeleted;
		this.text = text;
	}

	public int getCommentID() {
		return commentID;
	}
	public int getPostID() {
		return postID;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUserName() {
		return userName;
	}

	public String getDate() {
		return date;
	}


	public String getDateOfChange() {
		return dateOfChange;
	}

	public void setDateOfChange(String dateOfChange) {
		this.dateOfChange = dateOfChange;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "Comment [commentID=" + commentID + ", postID=" + postID + ", userName=" + userName + ", text=" + text
				+ ", date=" + date + ", dateOfChange=" + dateOfChange + ", isDeleted=" + isDeleted + "]";
	}
	public String toCSV() {
		//postID;commentID;userName;text;date;dateOfChange;isDeleted
		return  postID + ";" + commentID + ";"+ userName + ";" + text + ";" + date + ";" + 
		 dateOfChange  + ";" +  isDeleted;
	}

	@Override
	public int hashCode() {
		return Objects.hash(commentID, postID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		return Objects.equals(commentID, other.commentID) && Objects.equals(postID, other.postID);
	}
	
	
	
	
}
