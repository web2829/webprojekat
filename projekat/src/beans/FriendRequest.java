package beans;

import java.util.Date;
import java.util.Objects;

import helpers.DateTimeManipulation;

public class FriendRequest {
	
	private int requestID;
	private String senderUsername;
	private String recieverUsername;
	private Status status;
	private String date;
	
	public FriendRequest() {
		super();
	}
	
	// new friend request
	public FriendRequest(int requestID, String senderUsername, String recieverUsername) {
		super();
		this.requestID = requestID;
		this.senderUsername = senderUsername;
		this.recieverUsername = recieverUsername;
		this.status = Status.WAITING;
		this.date = DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate());
	}
	
	// read from file
	public FriendRequest(int requestID, String senderUsername, String recieverUsername, Status status, String date) {
		super();
		this.requestID = requestID;
		this.senderUsername = senderUsername;
		this.recieverUsername = recieverUsername;
		this.status = status;
		this.date = date;
	}
	
	public int getRequestID() {
		return requestID;
	}
	
	public String getSenderUsername() {
		return senderUsername;
	}
	public String getRecieverUsername() {
		return recieverUsername;
	}
	public void setSenderUsername(String senderUsername) {
		this.senderUsername = senderUsername;
	}
	public void setRecieverUsername(String recieverUsername) {
		this.recieverUsername = recieverUsername;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getDate() {
		return date;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(requestID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendRequest other = (FriendRequest) obj;
		return requestID == other.requestID;
	}

	public String toCSV() {

		return requestID +";" +senderUsername + ";" + recieverUsername + ";" + status.toString() +";" +date;
		
	}
	
	
}
