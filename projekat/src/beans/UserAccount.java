package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import helpers.DateTimeManipulation;

public class UserAccount {
	
	//private User userDetails;
	
	private String firstName;
	private String lastName;
	private String birthDate = "null";
	private Sex sex;
	private String email;
	
	private String username; // id
	private String password;
	private Role role;
	private boolean privateProfile = true;
	private String profilePicture; // path?
	//private BufferedImage image;
	//this.image = ImageIO.read(new File(name + ".png"));
	private boolean isDeleted;
	private boolean isBlocked;

	private ArrayList<Post> myPosts;
	private ArrayList<Post> myImages;
	
	private ArrayList<FriendRequest> pendingSentFriendRequests;
	private ArrayList<FriendRequest> pendingRecievedFriendRequests;
	private ArrayList<String> friends;
	private HashMap<String, ArrayList<DM>> chats;
	
	public UserAccount() {
	}
	
	// for creating new account
	public UserAccount(String firstName, String lastName, String birthDate, Sex sex, String email, String username, String password, boolean isPrivate) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate= birthDate;
		this.sex = sex;
		this.email = email;
		
		this.pendingSentFriendRequests = new  ArrayList<FriendRequest>();
		this.pendingRecievedFriendRequests = new  ArrayList<FriendRequest>();
		this.myPosts = new ArrayList<Post>();
		this.myImages = new ArrayList<Post>();
		this.friends = new ArrayList<String>();
		this.chats = new HashMap<String, ArrayList<DM>>();
		this.isDeleted = false;
		this.isBlocked = false;
		this.role = Role.USER;	// impossible to create new admin
		this.profilePicture =  "default_profile_pic"; // can be added later

		this.username = username;
		this.password = password;
		this.privateProfile = isPrivate;
		
		
	}
	
	// for reading from file
	public UserAccount( String firstName, String lastName, String birthDate, Sex sex, String email, 
			String username, String password, Role role, String profilePicture,  boolean isPrivate, boolean isDeleted, boolean isBlocked,
			ArrayList<FriendRequest> sentFriendRequests,ArrayList<FriendRequest> recievedFriendRequests, 
			ArrayList<String> friends, HashMap<String, ArrayList<DM>> chats, ArrayList<Post> myPosts, ArrayList<Post> myImages) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate= birthDate;
		this.sex = sex;
		this.email = email;
		
		this.username = username;
		this.password = password;
		this.role = role;
		this.profilePicture = profilePicture;
		this.pendingSentFriendRequests = sentFriendRequests;
		this.pendingRecievedFriendRequests = recievedFriendRequests;
		this.friends = friends;
		this.privateProfile = isPrivate;
		this.isDeleted = isDeleted;
		this.myPosts = myPosts;
		this.isBlocked = isBlocked;
		this.chats = chats;
		this.myImages = myImages;
	}
	public HashMap<String, ArrayList<DM>> getChats(){
		return chats;
	}
	
	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public ArrayList<FriendRequest> getPendingSentFriendRequests() {
		return pendingSentFriendRequests;
	}
	public ArrayList<FriendRequest> getPendingRecievedFriendRequests() {
		return pendingRecievedFriendRequests;
	}

	public ArrayList<String> getFriends() {
		return friends;
	}


	public boolean isPrivateProfile() {
		return privateProfile;
	}

	public void setPrivateProfile(boolean privateProfile) {
		this.privateProfile = privateProfile;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public ArrayList<Post> getMyPosts() {
		return myPosts;
	}

	public void setMyPosts(ArrayList<Post> myPosts) {
		this.myPosts = myPosts;
	}

	public String toCSV() {
		return  firstName + ";" + lastName + ";" + birthDate + ";" + sex + ";" + email + ";" +  username +";" + password +";"+ role +";"+ 
				privateProfile+";"+ profilePicture +";"+ isDeleted +";"+ isBlocked;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDateStr) {
		this.birthDate = birthDateStr;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public ArrayList<Post> getMyImages(){
		return myImages;
	}
	
		
	
	
}
