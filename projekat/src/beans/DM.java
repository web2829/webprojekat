package beans;


import java.util.Date;

import helpers.DateTimeManipulation;

public class DM {
	private int ID;
	private String senderUsername;
	private String recieverUsername;
	private String message;
	private String date;
	
	public DM() {
	}
	// new User
	public DM(int ID, String senderUsername, String recieverUsername, String message) {
		super();
		this.ID = ID;
		this.senderUsername = senderUsername;
		this.recieverUsername = recieverUsername;
		this.message = message;
		this.date = DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate());
	}
	
	// read from the file
	public DM(int ID, String senderUsername, String recieverUsername, String message, String date) {
		super();
		this.ID = ID;
		this.senderUsername = senderUsername;
		this.recieverUsername = recieverUsername;
		this.message = message;
		this.date = date;
	}
	
	public int getID() {
		return ID;
	}

	public String getSenderUsername() {
		return senderUsername;
	}

	public String getRecieverUsername() {
		return recieverUsername;
	}
	
	public void setSenderUsername(String senderUsername) {
		this.senderUsername = senderUsername;
	}

	public void setRecieverUsername(String recieverUsername) {
		this.recieverUsername=recieverUsername ;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String toCSV() {
		//DMID;senderUsername;recieverUsername;message;date
		return ID + ";" + senderUsername + ";" + recieverUsername + ";" + message  + ";" + date; 
	}
	
}
