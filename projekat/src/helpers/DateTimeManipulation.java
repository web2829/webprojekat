package helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeManipulation {
	
	//public static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    public static String dateToString(Date date) {
    	try {
    		if(date == null) {
    			return "null";
    		}
			return sdf.format(date);
		} 
    	catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
    /*
     * 		
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy.");
		String dateStr = "21.01.2022.";
		Date dateFromStr = null;
		
		System.out.println(formatter.format(dateFromStr));
     * */
    
    public static Date stringToDate(String dateStr) {
    	try {
    		if(dateStr==null) {
    			return null;
    		}
			return sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    public static Date getCurrentDate() {
    	return new Date();
    }
}
