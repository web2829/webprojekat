package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import beans.Comment;
import beans.Post;
import beans.TypeOfPost;
import beans.UserAccount;
import helpers.DateTimeManipulation;

public class PostDAO {
	private HashMap<Integer, Post> postsByID = new HashMap<Integer, Post>();
	private HashMap<String, ArrayList<Post>> postsByUsernames = new HashMap<String, ArrayList<Post>>();
	public CommentDAO commentDAO;
	
	//private String path = "./static/data/posts.txt";
	private String path = "posts.txt";
	
	private static int lastPostID = -1;
	
	public PostDAO() {
	}
	
	public PostDAO(String path ) {
		this.path = path + this.path;
		commentDAO = new CommentDAO(path);
		loadPosts();
	}
	
	//  FILTER/GETTER OPERATIONS  //
	
	public ArrayList<Post> getPostsByUsername(String username) {
		if(postsByUsernames.containsKey(username)) {
			return postsByUsernames.get(username);
		}
		return null;
	}
	
	public Post getPostByID(int postID) {
		if(postsByID.containsKey(postID)) {
			return postsByID.get(postID);
		}
		return null;
	}
	
	// filter comments
	public ArrayList<Comment> getNonDeletedComments(Post post){
		return filterNonDeletedComments(post.getComments());
	}
	
	public ArrayList<Comment> filterNonDeletedComments(Collection<Comment> allComments){
		if (allComments == null) {
			return new ArrayList<Comment>();
		}
		
		ArrayList<Comment> coms = (ArrayList<Comment>) allComments.stream()
				.filter(c -> !c.isDeleted())
				.collect(Collectors.toList());
		
		return coms;
	}
	
	// filter posts
	public ArrayList<Post> getAllNonDeletedPosts(){
		return filterNonDeletedPosts(postsByID.values());
	}
	
	public ArrayList<Post> filterNonDeletedPosts(Collection<Post> allPosts){
		ArrayList<Post> nonDeletedPosts = new ArrayList<Post>();
		for (Post post : allPosts) {
			if(!post.isDeleted()) {
				nonDeletedPosts.add(post);
			}
		}
		return nonDeletedPosts;
	}
	
	public  ArrayList<Post> filterPostsByType(Collection<Post> allPosts, TypeOfPost typeOfPost){
		ArrayList<Post> pictures = new ArrayList<Post>();
		for (Post post : allPosts) {
			if(post.getTypeOfPost() == typeOfPost) {
				pictures.add(post);
			}
		}
		return pictures;
	}
	
	
	
	//   CRUD OPERATIONS  //
	
	// comment
	public Comment addComment(Post post,  String username, String text) {
		if(post == null) {
			return null;
		}
		Comment comment = commentDAO.addComment(post, username, text);
		post.getComments().add(comment);
		return comment;
	}
	
	public boolean deleteComment(Comment comment) {
		if(comment == null) {
			return false;
		}
		commentDAO.deleteComment(comment);
		return true;
	}
	
	
	// post
	public boolean deletePost(Post post) {
		if(post == null) {
			return false;
		}
		post.setDeleted(true);
		commentDAO.deleteAllComments(post);
		saveChanges();
		return true;
	}
	
	public Post editPost(Post post, String newText) {
		post.setText(newText);
		saveChanges();
		return post;
	}

	public Post addPost(UserAccount user, Post post) {		
		
		int postID = generateID();
		post.setPostID(postID);
		
		post.setComments(new ArrayList<Comment>());
		post.setDeleted(false);
		post.setDate(DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate()));
		
		if(post.getTypeOfPost()==TypeOfPost.PICTURE) {
			if(post.getPhoto().equals("")) {
				return null;
			}
			user.getMyImages().add(post);
		}
		if(post.getTypeOfPost()==TypeOfPost.POST) {
			if(post.getText().equals("")) {
				return null;
			}
			user.getMyPosts().add(post);
		}
		
		storePostByID(post, postID);
		//storePostByUsername(post, user.getUsername());
		
		saveChanges();
		
		return post;
	}
	
	//  HELPER METHODS   // 
	private int generateID() {
		lastPostID++;
		return lastPostID;
	}
	
	private void storePostByID(Post post, int postID) {
		// add post byId
		if(!postsByID.containsKey(postID)) {
			postsByID.put(postID, post);
		}
		else {
			//TODO: throw exception?
		}
	}
	private void storePostByUsername(Post post, String username) {
		// add post for specific user
		if(postsByUsernames.containsKey(username)) {
			postsByUsernames.get(username).add(post);
		}
		// first post for this user
		else {
			ArrayList<Post> posts = new ArrayList<Post>();
			posts.add(post);
			postsByUsernames.put(username, posts);
		}
	}
	
	//   LOAD/SAVE METHODS   //
	private void parsePost(String[] lineParts) {
		
		//(int postID, String username, String photo, String text, ArrayList<Comment> comments, boolean isDeleted)
		String postIDStr = lineParts[0];
		String username = lineParts[1];
		String photo = lineParts[2];
		String text = lineParts[3];
		String isDeletedStr = lineParts[4];
		String typeStr = lineParts[5];
		String dateStr = lineParts[6];

		boolean isDeleted = true;
		int postID = Integer.parseInt(postIDStr);
		if(isDeletedStr.equals("false")) { isDeleted = false; }
		TypeOfPost type = TypeOfPost.valueOf(typeStr);
		Date date = DateTimeManipulation.stringToDate(dateStr);
		
		ArrayList<Comment> allComments = commentDAO.getCommentsByPostID(postID);
		
		ArrayList<Comment> nonDeletedComments = filterNonDeletedComments(allComments);  
		
		if(lastPostID < postID) {
			lastPostID = postID;
		}
		
		Post post = new Post(postID, username, photo, text, isDeleted, type, dateStr, nonDeletedComments);
		
		storePostByID(post, postID);
		storePostByUsername(post, username);
	}
	
	private void loadPosts() {
		BufferedReader in = null;
		try {
			File file = new File(path);
			in = new BufferedReader(new FileReader(file));
			String line;
			//StringTokenizer st;
			line = in.readLine();
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				//st = new StringTokenizer(line, ";");
//				while (st.hasMoreTokens()) {
				System.out.println(line);
					parsePost(line.split(";"));
//				}
					
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void saveChanges() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(path));
			out.println("postID;userName;photo;text;isDeleted;typeOfPost;date");
			for (Post post : this.postsByID.values()) {
				out.println(post.toCSV());
			}
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	}
}
