package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.Comment;
import beans.Post;
import helpers.DateTimeManipulation;

public class CommentDAO {
	
	
	private HashMap<Integer, ArrayList<Comment>> commentsByPost = new HashMap<Integer, ArrayList<Comment>>();
	
	//private String path = "./static/data/comment.txt";
	private String path = "comment.txt";
	
	private static int lastCommentID = -1;
	public CommentDAO() {
	}
	
	public CommentDAO(String path) {
		this.path = path + this.path;
		loadComments();
	}
	
	// users comments
	public ArrayList<Comment> getUserComment(String username){
		ArrayList<Comment> myComments = new ArrayList<Comment>();
		for (ArrayList<Comment> comments : commentsByPost.values()) {
			for (Comment comment : comments) {
				if(comment.getUserName().equals(username)) {
					myComments.add(comment);
				}
			}
		}
		return myComments;
	}
	// comment by comment id
	public Comment getCommentByID(int commentID) {
		for (ArrayList<Comment> comments : commentsByPost.values()) {
			for (Comment comment : comments) {
				if(comment.getCommentID() == commentID) {
					return comment;
				}
			}
		}
		return null;
	}
	
	// iz post-a
	public ArrayList<Comment> getCommentsByPostID(int postID) {
		if(commentsByPost.containsKey(postID)) {
			return commentsByPost.get(postID);
		}
		return null;
	}
	
	public void deleteAllComments(Post post) {
		for (Comment comment : post.getComments()) {
			comment.setDeleted(true);
		}
		saveChanges();
	}
	
	public void deleteComment(Comment comment) {
		comment.setDeleted(true);
		saveChanges();
	}
	public Comment editComment(Comment comment, String newText) {
		comment.setText(newText);
		comment.setDateOfChange(DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate()));
		saveChanges();
		return comment;
	}
	
	// to be called from postDAO
	public Comment addComment(Post post,  String userName, String text) {
		if(userName.equals("") ||	text.equals("")){
			return null;
		}
		int postID = post.getPostID();
		int commentID = generateID();
		
		Comment comment = new Comment(postID, commentID, userName, text);
		storeComment(comment, postID);

		saveChanges();
		
		return comment;
	}
	
	private int generateID() {
		lastCommentID++;
		return lastCommentID;
	}


	
	private void storeComment(Comment comment, int postID) {
		// add comment for specific post
		if(commentsByPost.containsKey(postID)) {
			commentsByPost.get(postID).add(comment);
		}
		
		// first comment for this post
		else {
			ArrayList<Comment> comments = new ArrayList<Comment>();
			comments.add(comment);
			commentsByPost.put(postID, comments);
		}
	}
	
	private void loadComments() {
		BufferedReader in = null;
		try {
			File file = new File(path);
			in = new BufferedReader(new FileReader(file));
			String line;
			StringTokenizer st;
			line = in.readLine();
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					parseComment(st);
				}
					
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void parseComment(StringTokenizer st) {
		String postIDStr = st.nextToken().trim();
		String commentIDStr = st.nextToken().trim();
		String userName = st.nextToken().trim();
		String text = st.nextToken().trim();
		
		String dateStr = st.nextToken().trim();
		String dateOfChangeStr = st.nextToken().trim();
		String isDeletedStr = st.nextToken().trim();
		
		Date date = DateTimeManipulation.stringToDate(dateStr);
		Date dateOfChange = null;
		boolean isDeleted = true;
		int commentID = Integer.parseInt(commentIDStr);
		int postID = Integer.parseInt(postIDStr);
				
		if(!dateOfChangeStr.equals("null")) {
			dateOfChange = DateTimeManipulation.stringToDate(dateOfChangeStr);
		}
		
		if(isDeletedStr.equals("false")) { isDeleted = false; }
		
		
		if(lastCommentID < commentID) {
			lastCommentID = commentID;
		}
		Comment comment = new Comment(postID, commentID, userName, text, dateStr, dateOfChangeStr, isDeleted);
		storeComment(comment, postID);
	}

	private void saveChanges() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(path));
			out.println("postID;commentID;userName;text;date;dateOfChange;isDeleted");
			for (ArrayList<Comment> comments : this.commentsByPost.values()) {
				for (Comment comment : comments) {
					out.println(comment.toCSV());
				}
			}
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	}

	
}
