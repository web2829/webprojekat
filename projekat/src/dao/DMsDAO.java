package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.DM;
import beans.UserAccount;
import helpers.DateTimeManipulation;

public class DMsDAO {
	
	private String path = "dms";
	// <firstUser,secondUser>, list<dms>>
	private HashMap<ArrayList<String>, ArrayList<DM>> groupedDMs = new HashMap<ArrayList<String>, ArrayList<DM>>();
	
	private static int latestID = -1;
	public DMsDAO() {
	}
	
	public DMsDAO(String path) {
		this.path = path + this.path;
		loadDMs();
	}

	private int generateID() {
		latestID++;
		return latestID;
	}
	
	public HashMap<String, ArrayList<DM>> getDMsByUser(String username){
		HashMap<String, ArrayList<DM>> myDMs = new HashMap<String, ArrayList<DM>>();
		for (HashMap.Entry<ArrayList<String>, ArrayList<DM>> entry : groupedDMs.entrySet()) {
			String firstUsername = entry.getKey().get(0);
			String secondUsername = entry.getKey().get(1);
			if(firstUsername.equals(username)) {
				myDMs.put(secondUsername, entry.getValue());
			}
			if(secondUsername.equals(username)) {
				myDMs.put(firstUsername, entry.getValue());
			}
		}
		return myDMs;
	}
	
	public DM addDM(UserAccount sender, UserAccount reciever, String message) {
		if(message.equals("")) {
			return null;
		}
		int ID = generateID();
		
		DM dm = new DM(ID, sender.getUsername() , reciever.getUsername(), message, DateTimeManipulation.dateToString(DateTimeManipulation.getCurrentDate()));

		ArrayList<DM> messages = addDMToUsers(sender, reciever, dm);
		
		ArrayList<String> users = new ArrayList<String>();
		users.add(reciever.getUsername());
		users.add(sender.getUsername());
		saveChangesInChat(users, messages);
		return dm;
	}
	
	private ArrayList<DM> addDMToUsers(UserAccount sender, UserAccount receiver, DM dm) {
		String receiverUsername = receiver.getUsername();
		String senderUsername = sender.getUsername();
		
		HashMap<String, ArrayList<DM>> senderChat = sender.getChats();
		HashMap<String, ArrayList<DM>> receiverChat = receiver.getChats();
		
		
		ArrayList<DM> messages;
		if(senderChat.containsKey(receiverUsername)) {
			messages = senderChat.get(receiverUsername);
			messages.add(dm);
		}
		else {
			messages = new ArrayList<DM>();
			messages.add(dm);
			senderChat.put(receiverUsername, messages);
		}
		
		if(!receiverChat.containsKey(senderUsername)) {
			receiverChat.put(senderUsername, messages);
		}		
		
		return messages;
	}
	
	private void loadDMs() {
		BufferedReader in = null;
		try {
			File dir = new File(path);
		    File[] directoryListing = dir.listFiles();
			if (directoryListing != null) {
			    for (File file : directoryListing) {
			    	//File file = new File(path);
			    	in = new BufferedReader(new FileReader(file));
					String line;
					StringTokenizer st;
					// skip first line
					line = in.readLine();
					
					ArrayList<String> usernames = new ArrayList<String>();
					ArrayList<DM> dms = new ArrayList<DM>();
					DM dm = null;
					// za svaki dm
					while ((line = in.readLine()) != null) {
						line = line.trim();
						if (line.equals("") || line.indexOf('#') == 0)
							continue;
						st = new StringTokenizer(line, ";");
						while (st.hasMoreTokens()) {
							
							dm = parseDMs(st);
							dms.add(dm);
						}
					}
					usernames.add(dm.getSenderUsername());
					usernames.add(dm.getRecieverUsername());
					groupedDMs.put(usernames, dms);
			    }
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private DM parseDMs(StringTokenizer st) {
		//DMID;senderUsername;recieverUsername;message;date
		String IDstr = st.nextToken().trim();
		String senderUsername = st.nextToken().trim();
		String recieverUsername = st.nextToken().trim();
		String message = st.nextToken().trim();
		String dateStr = st.nextToken().trim();
		
		Date date = DateTimeManipulation.stringToDate(dateStr);
		int ID = Integer.parseInt(IDstr);
		
		if(latestID < ID) {
			latestID = ID;
		}
		
		DM dm = new DM(ID, senderUsername, recieverUsername, message, dateStr);
		return dm;
		
	}

	private void saveChangesInChat(ArrayList<String> users, ArrayList<DM> chat) {
		try {
			String filename1 = users.get(0) + "_" + users.get(1) + ".txt";
			File file = new File(path + "/"+filename1);
			
			if (!file.exists()) {
				String filename2 = users.get(1) + "_" + users.get(0) + ".txt";
				file = new File(path + "/"+filename2);
				
				if (!file.exists()) 
				{ file.createNewFile(); }
			}
			
			PrintStream out = new PrintStream(new FileOutputStream(file));
			
			out.println("DMID;senderUsername;recieverUsername;message;date");
			
			for (DM dm : chat) {
				out.println(dm.toCSV());
			}
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	}
	
}
