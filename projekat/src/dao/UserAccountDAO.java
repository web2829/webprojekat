package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.DM;
import beans.FriendRequest;
import beans.Post;
import beans.Role;
import beans.Sex;
import beans.Status;
import beans.TypeOfPost;
import beans.UserAccount;
import helpers.DateTimeManipulation;

public class UserAccountDAO {
	
	//private ArrayList<UserAccount> users = new ArrayList<UserAccount>();
	private HashMap<String, UserAccount> users = new HashMap<String, UserAccount>();

	//private String path = "./WebContent/static/data/users.txt";
	//private String path = "./static/data/users.txt";
	private String path = "users.txt";
	
	public FriendRequestDAO friendRequestDAO;
	public DMsDAO dmDAO;
	public PostDAO postDAO;
	private static UserAccountDAO singleton;
	
	static public UserAccountDAO getUserAccountDAO() {
		if (singleton == null) {
			singleton = new UserAccountDAO();
		} 
		
		return singleton;
	}
	
	private UserAccountDAO() {
		String usingAbsPath = AbsPath.absPath; 
		this.path = AbsPath.absPath + this.path;
		friendRequestDAO  = new FriendRequestDAO(usingAbsPath);
		dmDAO = new DMsDAO(usingAbsPath); 
		postDAO = new PostDAO(usingAbsPath);
		loadUsers();
	}
	
	// GETTER / FILTER OPERATIONS
	public UserAccount getUserAccountByUsername(String username) {
		return users.get(username);
	}
	
	public ArrayList<UserAccount> getUsersByUsernamesList(Collection<String> usernames){
		ArrayList<UserAccount> userAccounts = new ArrayList<UserAccount>();
		for (String username : usernames) {
			userAccounts.add(users.get(username));
		}
		return userAccounts;
	}
	
	public UserAccount find(String username, String password) {
		UserAccount user = getUserAccountByUsername(username);
		if(user== null) {
			return null;
		}
		if (!user.getPassword().equals(password)) {
			return null;
		}
		return user;
	}
	
	public Collection<UserAccount> getAllUsers(){
		return users.values();
	}
	public Collection<UserAccount> getAllNonDeletedUsers(){
		return filterNonDeletedUsers(this.users.values());
	}
	
	public Collection<UserAccount> filterNonDeletedUsers(Collection<UserAccount> users){
		ArrayList<UserAccount> filtered = new ArrayList<UserAccount>();
		for (UserAccount userAccount : users) {
			if(userAccount.isDeleted()) {
				filtered.add(userAccount);
			}
		}
		return filtered;
	}
	
	public UserAccount changeUser(UserAccount toBeChanged, UserAccount changes) {
		if(!changes.getFirstName().equals("")) {
			toBeChanged.setFirstName(changes.getFirstName());
		}
		if(!changes.getLastName().equals("")) {
			toBeChanged.setLastName(changes.getLastName());
		}
		if(!changes.getPassword().equals("")) {
			toBeChanged.setPassword(changes.getPassword());
		}
		if(!changes.getBirthDate().equals("")) {
			toBeChanged.setBirthDate(changes.getBirthDate());
		}
		if(!changes.getEmail().equals("")) {
			toBeChanged.setEmail(changes.getEmail());
		}
		toBeChanged.setPrivateProfile(changes.isPrivateProfile());
		saveChanges();
		return toBeChanged;
	}
	
	
	public void blockUser(UserAccount user) {
		user.setBlocked(true);
		saveChanges();
	}
	
	public void unblockUser(UserAccount user) {
		user.setBlocked(false);
		saveChanges();
	}
	
	//  POST MANIPULATION   ///
	public boolean deletePost(Post post, UserAccount user) {
		if(post.getTypeOfPost()==TypeOfPost.PICTURE) {
			user.getMyImages().remove(post);
		}
		else if(post.getTypeOfPost()==TypeOfPost.POST) {
			user.getMyPosts().remove(post);
		}
		return postDAO.deletePost(post);
	}
	
	public Post addPost(UserAccount user, Post post) {
		if(post.getTypeOfPost()==TypeOfPost.PICTURE) {
			user.getMyImages().add(post);
		}
		else if(post.getTypeOfPost()==TypeOfPost.POST) {
			user.getMyPosts().add(post);
		}
		return postDAO.addPost(user, post);
	}
	
	
	//    REQUESTS MANIPULATION    //
	public boolean changeRequestStatus(FriendRequest request, Status status) {
		boolean changed =  friendRequestDAO.changeRequestStatus(request, status);
		if(!changed) {
			return false;
		}
		String senderUsername =  request.getSenderUsername();
		String recieverUsername = request.getRecieverUsername();
		
		UserAccount sender = getUserAccountByUsername(senderUsername);
		UserAccount reciever = getUserAccountByUsername(recieverUsername);
		
		if(!sender.getPendingSentFriendRequests().remove(request) ||
		   !reciever.getPendingRecievedFriendRequests().remove(request)) 
			
		{ return false; }
		
		if(status==Status.ACCEPTED ) {
			sender.getFriends().add(recieverUsername);
			reciever.getFriends().add(senderUsername);
		}
		
		return true;
	}
	
	public FriendRequest sendRequest(UserAccount sender, String recieverUsername) {
		UserAccount reciever = getUserAccountByUsername(recieverUsername);
		if(reciever == null) {
			return null;
		}
		if(sender.getFriends().contains(recieverUsername)) {
			return null;	// cannot add friend if already friends
		}
		return friendRequestDAO.addRequest(sender, reciever);
	}
	
	public boolean removeFriend(UserAccount firstUser, String secondUsername) {
		UserAccount secondUser = getUserAccountByUsername(secondUsername);
		boolean success = true;
		success &= firstUser.getFriends().remove(secondUsername);
		success &= secondUser.getFriends().remove(firstUser.getUsername());
		return success;
	}
	public ArrayList<String> mutualFreinds(UserAccount loggedUser, String otherUsername) {
		ArrayList<String> mutuals = new ArrayList<String>();
		UserAccount otherUser = getUserAccountByUsername(otherUsername);
		for (String user : loggedUser.getFriends()) {
			if(otherUser.getFriends().contains(user)) {
				mutuals.add(user);	
			}
			
		}
		return mutuals;
	}
	
	public boolean isRequestSent(UserAccount firstUser, String secondUser) {
		for (FriendRequest fr : firstUser.getPendingSentFriendRequests()) {
			if(fr.getRecieverUsername().equals(secondUser)) {
				return true;
			}
		}
		for (FriendRequest fr : firstUser.getPendingRecievedFriendRequests()) {
			if(fr.getSenderUsername().equals(secondUser)) {
				return true;
			}
		}
		return false;
	}

	
	//   DM MANIPULATION   //
	// Do I have to be friends with reciever to send DM?
	public DM sendDM(DM dm) {
		UserAccount sender = getUserAccountByUsername(dm.getSenderUsername());
		UserAccount reciever = getUserAccountByUsername(dm.getRecieverUsername());
		if(reciever==null) {
			return null;	
		}
		if(sender.getRole()==Role.ADMIN) {
			// TODO: URGENT MESSAGE?
		}
		
		return dmDAO.addDM(sender, reciever, sanitize(dm.getMessage()));
	}
	
	public DM sendAdminReasonDM(UserAccount admin, UserAccount user, Post post,String reason) {
		if(admin==null||user==null) {
			return null;	
		}
		
		String message = "Your post has been deleted. The reason: " + reason + ". Sincerely, Admin: " + admin.getFirstName() + " " + admin.getLastName();
		
		return dmDAO.addDM(admin, user, message);
	}
	
	private String sanitize(String message) {
		return message.replace("\n", " ");
	}

	
	//  USER&ACCOUNT MANIPULATION   //
	public UserAccount addUser(String name, String lastName, String birthDate, Sex sex, String email, String username, String password, boolean isPrivate) {
		if(usernameExists(username)) {
			return null; // TODO: exception
		}
		UserAccount user = new UserAccount(name, lastName, birthDate, sex, email, username,password, isPrivate);
		users.put(username, user);
		saveChanges();
		return user;
	}
	
	public UserAccount changeProfilePic(UserAccount user, String picPath) {
		user.setProfilePicture(picPath);
		saveChanges();
		return user;
	}
	
	
	
	private boolean usernameExists(String username) {
		if(getUserAccountByUsername(username)==null) {
			return false;
		}
		return true;
	}

	//  LOAD / STORE OPERATIONS   //
	private void loadUsers() {
		BufferedReader in = null;
		try {
			File file = new File(path);
			in = new BufferedReader(new FileReader(file));
			String line;
			StringTokenizer st;
			line = in.readLine();
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					parseUser(st);
				}
					
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void parseUser(StringTokenizer st) {
		//name;lastName;birthDate;sex;email; username;password;  role;isPrivate;profilePicture;isDeleted;isBlocked
		String name = st.nextToken().trim();
		String lastName = st.nextToken().trim();
		String birthDate = st.nextToken().trim();
		String sexStr = st.nextToken().trim();
		String email = st.nextToken().trim();
		
		Date date = null;
		if(!birthDate.equals("null")) {
			date = DateTimeManipulation.stringToDate(birthDate);
		}
		 
		Sex sex = Sex.valueOf(sexStr);
		
		String username = st.nextToken().trim();
		String password = st.nextToken().trim();
		String roleStr = st.nextToken().trim();
		String isPrivateStr = st.nextToken().trim();
		String profilePicture = st.nextToken().trim();
		String isDeletedStr = st.nextToken().trim();
		String isBlockedStr = st.nextToken().trim();

		Role role = Role.valueOf(roleStr);
		
		boolean isDeleted = true;
		if(isDeletedStr.equals("false")) { isDeleted = false; }
		boolean isPrivate = true;
		if(isPrivateStr.equals("false")) { isPrivate = false; }
		boolean isBlocked = true;
		if(isBlockedStr.equals("false")) { isBlocked = false; }

		ArrayList<FriendRequest> pendingSentFriendRequests = getMySentRequests(username);
		ArrayList<FriendRequest> pendingRecievedFriendRequests = getMyRecievedRequests(username);
		
		HashMap<String, ArrayList<DM>> chats = dmDAO.getDMsByUser(username);
		
		ArrayList<String> friends = friendRequestDAO.getMyFriends(username);
		if(friends==null) {
			friends = new ArrayList<String>();
		}
		
		ArrayList<Post> allMyPosts = postDAO.getPostsByUsername(username);
		if(allMyPosts==null) {
			allMyPosts = new ArrayList<Post>();
		}
		ArrayList<Post> allMyNonDeletedPosts = postDAO.filterNonDeletedPosts(allMyPosts);
		
		ArrayList<Post> myNonDeletedImages = postDAO.filterPostsByType(allMyNonDeletedPosts, TypeOfPost.PICTURE);
		ArrayList<Post> myNonDeletedPosts = postDAO.filterPostsByType(allMyNonDeletedPosts, TypeOfPost.POST);
				
		UserAccount user = new UserAccount(name,lastName,birthDate,sex,email,
										   username, password, role, profilePicture, isPrivate, isDeleted, isBlocked, 
										   pendingSentFriendRequests, pendingRecievedFriendRequests,
										   friends, chats, myNonDeletedPosts, myNonDeletedImages);
		users.put(username, user);
	}
	

	private ArrayList<FriendRequest> getMyRecievedRequests(String username) {
		ArrayList<FriendRequest> recievedFriendRequests = friendRequestDAO.getAllRequestsByReciever(username);
		ArrayList<FriendRequest> pendingRecievedFriendRequests;
		if(recievedFriendRequests== null) {
			pendingRecievedFriendRequests = new ArrayList<FriendRequest>();
		}
		else {
			pendingRecievedFriendRequests = friendRequestDAO.filterPendingRequests(recievedFriendRequests);	
		}
		return pendingRecievedFriendRequests;
	}

	private ArrayList<FriendRequest> getMySentRequests(String username) {
		ArrayList<FriendRequest> sentFriendRequests = friendRequestDAO.getAllRequestsBySender(username);
		ArrayList<FriendRequest> pendingSentFriendRequests;
		if(sentFriendRequests== null) {
			pendingSentFriendRequests = new ArrayList<FriendRequest>();
		}
		else {
			pendingSentFriendRequests = friendRequestDAO.filterPendingRequests(sentFriendRequests);	
		}
		return pendingSentFriendRequests;
	}

	private void saveChanges() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(path));
			out.println("name;lastName;birthDate;sex;email; username;password;  role;isPrivate;profilePicture;isDeleted;isBlocked");
			for (UserAccount user : this.users.values()) {
				out.println(user.toCSV());
			}
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	}

	

	
	
}
