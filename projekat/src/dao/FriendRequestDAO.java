package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.FriendRequest;
import beans.Status;
import beans.UserAccount;
import helpers.DateTimeManipulation;

public class FriendRequestDAO {
	
	private ArrayList<FriendRequest> requests = new ArrayList<FriendRequest>();
	private HashMap<String,  ArrayList<FriendRequest>> requestsBySenders = new HashMap<String,  ArrayList<FriendRequest>>();
	private HashMap<String,  ArrayList<FriendRequest>> requestsByRecievers = new HashMap<String,  ArrayList<FriendRequest>>();
	
	private HashMap<String,  ArrayList<String>> friendships = new HashMap<String,  ArrayList<String>>();

	private String path = "friendRequests.txt";
	
	private static int latestID = -1;
	
	public FriendRequestDAO() {
	}
	
	public FriendRequestDAO(String path) {
		this.path = path + this.path;
		loadRequests();
		groupRequests();
	}
	// GROUP / STORE OPERATIONS
	private void groupRequests() {
		for (FriendRequest friendRequest : requests) {
			String reciever = friendRequest.getRecieverUsername();
			String sender = friendRequest.getSenderUsername();
			
			// senders
			if(requestsBySenders.containsKey(sender)) {
				requestsBySenders.get(sender).add(friendRequest);
			}
			else {
				 ArrayList<FriendRequest> r = new ArrayList<FriendRequest>();
				 r.add(friendRequest);
				 requestsBySenders.put(sender, r);
			}
			
			// recievers
			if(requestsByRecievers.containsKey(reciever)) {
				requestsByRecievers.get(reciever).add(friendRequest);
			}
			else {
				 ArrayList<FriendRequest> r = new ArrayList<FriendRequest>();
				 r.add(friendRequest);
				 requestsByRecievers.put(reciever, r);
			}
			
			// friendships
			if(friendRequest.getStatus().equals(Status.ACCEPTED)) {
				// sender
				if(friendships.containsKey(sender)) {
					friendships.get(sender).add(reciever);
				}
				else {
					ArrayList<String> r = new ArrayList<String>();
					 r.add(reciever);
					 friendships.put(sender, r);
				}
				// reciever
				if(friendships.containsKey(reciever)) {
					friendships.get(reciever).add(sender);
				}
				else {
					ArrayList<String> r = new ArrayList<String>();
					 r.add(sender);
					 friendships.put(reciever, r);
				}
			}
		}
		
	}

	
	//  GETTER OPERATIONS   ///
	public ArrayList<FriendRequest> getAllRequestsByReciever(String recieverUsername){
		return requestsByRecievers.get(recieverUsername);
	}
	
	public ArrayList<FriendRequest> getAllRequestsBySender(String senderUsername){
		return requestsBySenders.get(senderUsername);
	}

	public FriendRequest getRequestByID(int requestID) {
		for (FriendRequest friendRequest : requests) {
			if(friendRequest.getRequestID()==requestID) {
				return friendRequest;
			}
		}
		return null;
	}
	public ArrayList<String> getMyFriends(String user){
		return friendships.get(user);
	}
	
	//   FILTER OPERATIONS  ///
	public ArrayList<FriendRequest> filterPendingRequests(Collection<FriendRequest> allRequests){
		ArrayList<FriendRequest> pendingRequests = new ArrayList<FriendRequest>();
		for (FriendRequest request : allRequests) {
			if(request.getStatus()==Status.WAITING) {
				System.out.println(request.toCSV());
				pendingRequests.add(request);
			}
		}
		return pendingRequests;
	}
	
	
	//   CRUD OPERATIONS   //
	public boolean changeRequestStatus(FriendRequest friendRequest, Status status) {
		if(friendRequest==null) {
			return false;
		}
		if(friendRequest.getStatus()!= Status.WAITING) {
			return false;
		}
		friendRequest.setStatus(status);
		saveChanges();
		return true;
	}

	public FriendRequest addRequest(UserAccount sender, UserAccount reciever) {
		FriendRequest request = new FriendRequest(generateID(), sender.getUsername(), reciever.getUsername());
		
		reciever.getPendingRecievedFriendRequests().add(request);
		sender.getPendingSentFriendRequests().add(request);
		
		requests.add(request);
		saveChanges();
		
		return request;
	}

	// HELPER OPERAIONS
	private int generateID() {
		latestID++;
		return latestID;
	}

	
	//   LOAD/SAVE OPERATIONS   //
	private void loadRequests() {
		BufferedReader in = null;
		try {
			File file = new File(path);
			in = new BufferedReader(new FileReader(file));
			String line;
			StringTokenizer st;
			line = in.readLine();
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					parseRequest(st);
				}
					
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void parseRequest(StringTokenizer st) {
		//senderUsername;recieverUsername;status;date
		String IDstr = st.nextToken().trim();
		String senderUsername = st.nextToken().trim();
		String recieverUsername = st.nextToken().trim();
		String statusStr = st.nextToken().trim();
		String dateStr = st.nextToken().trim();
		
		Status status = Status.valueOf(statusStr);
		Date date = DateTimeManipulation.stringToDate(dateStr);
		int ID = Integer.parseInt(IDstr);
		FriendRequest request = new FriendRequest(ID, senderUsername, recieverUsername, status, dateStr);
		
		if(latestID < ID) {
			latestID = ID;
		}
		requests.add(request);
	}

	private void saveChanges() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(path));
			out.println("senderUsername;recieverUsername;status;date");
			for (FriendRequest request : this.requests) {
				out.println(request.toCSV());
			}
			out.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	}
	
	
}
