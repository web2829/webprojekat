package services;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import beans.UserAccount;
import dao.UserAccountDAO;
import helpers.DateTimeManipulation;

@Path("/UserService")
public class UserService {
	
	@Context
	ServletContext ctx;
	
	public UserService() {
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null) {
			ctx.setAttribute("userDAO", UserAccountDAO.getUserAccountDAO());
		}
	}
	
	@GET
	@Path("/user/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount login(@PathParam("username") String username) {
		System.out.println(username);
		System.out.println(((UserAccountDAO) ctx.getAttribute("userDAO")).getUserAccountByUsername(username).toCSV());
		return ((UserAccountDAO) ctx.getAttribute("userDAO")).getUserAccountByUsername(username);
	}
	
	@GET
	@Path("/getProfilePic/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount getProfilePic(@PathParam("username")String username, @Context HttpServletRequest request) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount user = userDAO.getUserAccountByUsername(username);
		return user;
	}
	
	@GET
	@Path("/searchName/{name}/{sortType}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchName(@PathParam("name")String name, @PathParam("sortType")String sortType) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		List<UserAccount> users = userDAO.getAllUsers().stream()
									.filter(u -> u.getFirstName().toLowerCase().startsWith(name.toLowerCase()))
									.collect(Collectors.toList());
		users = sort(users, sortType);
		ResponseBuilder response = Response.status(200).entity((Object) users); 
	    
        return response.build();
	}
	
	@GET
	@Path("/searchLastName/{lastName}/{sortType}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchLastName(@PathParam("lastName")String lastName, @PathParam("sortType")String sortType) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		List<UserAccount> users = userDAO.getAllUsers().stream()
									.filter(u -> u.getLastName().toLowerCase().startsWith(lastName.toLowerCase()))
									.collect(Collectors.toList());
		users = sort(users, sortType);
		ResponseBuilder response = Response.status(200).entity((Object) users); 
	    
        return response.build();
	}
	
	@GET
	@Path("/searchEmail/{email}/{sortType}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchEmail(@PathParam("email")String email, @PathParam("sortType")String sortType) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		List<UserAccount> users = userDAO.getAllUsers().stream()
									.filter(u -> u.getEmail().toLowerCase().startsWith(email.toLowerCase()))
									.collect(Collectors.toList());
		users = sort(users, sortType);
		ResponseBuilder response = Response.status(200).entity((Object) users); 
	    
        return response.build();
	}
	
	@GET
	@Path("/searchBirthdate/{fromDate}/{toDate}/{sortType}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchBirthdate(@PathParam("fromDate")String fromDate, 
			@PathParam("toDate")String toDate,
			@PathParam("sortType")String sortType) {
		Date min = parseDate(fromDate);
		Date max = parseDate(toDate);
		
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		List<UserAccount> users = userDAO.getAllUsers().stream()
									.filter(u -> DateTimeManipulation.stringToDate(u.getBirthDate()).after(min) 
											&& DateTimeManipulation.stringToDate(u.getBirthDate()).before(max))
									.collect(Collectors.toList());
		
		users = sort(users, sortType);
		ResponseBuilder response = Response.status(200).entity((Object) users); 
	    
        return response.build();
	}
	
	@GET
	@Path("/searchAll/{sortType}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchAll(@PathParam("email")String email, @PathParam("sortType")String sortType) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		List<UserAccount> users = userDAO.getAllUsers().stream().collect(Collectors.toList());
		users = sort(users, sortType);
		ResponseBuilder response = Response.status(200).entity((Object) users); 
	    
        return response.build();
	}
	
	private List<UserAccount> sort(List<UserAccount> users, String sortType) {
		switch(sortType) {
		  case "NAME":
			  return sortName(users);
		  case "LASTNAME":
			  return sortLastName(users);
		  case "BIRTHDATE":
			  return sortBirthDate(users);
		  default:
			  return null;
		}
	}
	
	private List<UserAccount> sortName(List<UserAccount> users) {
		return users.stream()
				.sorted(Comparator.comparing(UserAccount::getFirstName))
				.collect(Collectors.toList());
	}
	
	private List<UserAccount> sortLastName(List<UserAccount> users) {
		return users.stream()
				.sorted(Comparator.comparing(UserAccount::getLastName))
				.collect(Collectors.toList());
	}
	
	private List<UserAccount> sortBirthDate(List<UserAccount> users) {
		users.sort(Comparator.comparing(o -> DateTimeManipulation.stringToDate(o.getBirthDate())));
		return users;
	}
	
	@SuppressWarnings("deprecation")
	private Date parseDate(String dateStr) {
		String[] dateParts = dateStr.split("\\.");
 		int day = Integer.parseInt(dateParts[0]);
 		int month = Integer.parseInt(dateParts[1]);
 		int year = Integer.parseInt(dateParts[2]);
		return new Date(year - 1900, month - 1, day);
	}
}
