package services;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Comment;
import beans.Post;
import beans.UserAccount;
import dao.CommentDAO;
import dao.PostDAO;
import dao.UserAccountDAO;

@Path("/detailedView")
public class DetailedViewService {

	@Context
	ServletContext ctx;
	
	public DetailedViewService() {
	}
	
	@PostConstruct
	public void init() {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		if (userDAO == null) {
	    	userDAO = UserAccountDAO.getUserAccountDAO();
			ctx.setAttribute("userDAO", userDAO);
		}
		PostDAO postDAO = (PostDAO) ctx.getAttribute("postDAO");
		if (postDAO == null) {
			postDAO = userDAO.postDAO;
			ctx.setAttribute("postDAO", postDAO);
		}
		CommentDAO commentDAO = (CommentDAO) ctx.getAttribute("commentDAO");
		if (commentDAO == null) {
			commentDAO = postDAO.commentDAO;
			ctx.setAttribute("commentDAO", commentDAO);
		}
	}
	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response selectedImage(Post selectedImage, @Context HttpServletRequest request) {
		if(selectedImage==null) {
			return Response.status(400).entity("Please select image").build();
		}
		PostDAO postDao = (PostDAO) ctx.getAttribute("postDAO");
		Post original = postDao.getPostByID(selectedImage.getPostID()); 
		request.getSession().setAttribute("selectedImage", original);
		return Response.status(200).build();
	}
	
	@GET
	@Path("/load")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Post selectedImage(@Context HttpServletRequest request) {
		return (Post) request.getSession().getAttribute("selectedImage");
	}
	
	@POST
	@Path("/addComment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Comment addComment(Comment comment, @Context HttpServletRequest request) {
		PostDAO postDao = (PostDAO) ctx.getAttribute("postDAO");
		Post post = (Post) request.getSession().getAttribute("selectedImage");
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		Comment newComment = postDao.addComment(post, loggedUser.getUsername(), comment.getText());
		return newComment;
	}
	
	@POST
	@Path("/editComment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Comment editComment(Comment comment, @Context HttpServletRequest request) {
		CommentDAO commentDao = (CommentDAO) ctx.getAttribute("commentDAO");
		Comment original = commentDao.getCommentByID(comment.getCommentID());
		return commentDao.editComment(original, comment.getText());
	}
	
	@POST
	@Path("/deleteComment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Comment deleteComment(Comment comment, @Context HttpServletRequest request) {
		CommentDAO commentDao = (CommentDAO) ctx.getAttribute("commentDAO");
		Comment original = commentDao.getCommentByID(comment.getCommentID());
		commentDao.deleteComment(original);
		return original;
	}
	
	@POST
	@Path("/editPost")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Post editPost(Post post, @Context HttpServletRequest request) {
		PostDAO postDao = (PostDAO) ctx.getAttribute("postDAO");
		Post original = postDao.getPostByID(post.getPostID());
		return postDao.editPost(original, post.getText());
	}
	
	@POST
	@Path("/deletePost")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Post deletePost(Post post, @Context HttpServletRequest request) {
		UserAccount user = (UserAccount) request.getSession().getAttribute("user");
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		
		PostDAO postDao = (PostDAO) ctx.getAttribute("postDAO");
		Post original = postDao.getPostByID(post.getPostID());
		
		userDAO.deletePost(original, user);
		return original;
	}
	@POST
	@Path("/deletePost/{reason}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Post deletePost(Post post, @PathParam("reason") String reason, @Context HttpServletRequest request) {
		UserAccount loggedAdmin = (UserAccount) request.getSession().getAttribute("user");
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount user = userDAO.getUserAccountByUsername(post.getUsername());
		
		PostDAO postDao = (PostDAO) ctx.getAttribute("postDAO");
		Post original = postDao.getPostByID(post.getPostID());
		
		userDAO.deletePost(original, user);
		userDAO.sendAdminReasonDM(loggedAdmin, user, post, reason);
		return original;
	}
	@POST
	@Path("/setAsProfilePic")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount setAsProfilePic(Post post, @Context HttpServletRequest request) {
		String username = post.getUsername();
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount user = userDAO.getUserAccountByUsername(username);
		
		return userDAO.changeProfilePic(user, post.getPhoto());
	}
	
}
