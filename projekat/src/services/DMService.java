package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Comment;
import beans.DM;
import beans.Post;
import beans.UserAccount;
import dao.DMsDAO;
import dao.PostDAO;
import dao.UserAccountDAO;

@Path("/DMService")
public class DMService {
	
	@Context
	ServletContext ctx;
	
	public DMService() {
	}
	
	@PostConstruct
	public void init() {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		if (userDAO == null) {
	    	userDAO = UserAccountDAO.getUserAccountDAO();
			ctx.setAttribute("userDAO", userDAO);
		}
		DMsDAO dmDAO = (DMsDAO) ctx.getAttribute("dmDAO");
		if (dmDAO == null) {
			dmDAO = userDAO.dmDAO;
			ctx.setAttribute("dmDAO", dmDAO);
		}
	}
	/*@POST
	@Path("/saveSelectedUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response selectedUser(UserAccount selectedUser, @Context HttpServletRequest request) {
		if(selectedUser==null) {
			return Response.status(400).entity("Please select user").build();
		}
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		
		UserAccount original = userDAO.getUserAccountByUsername(selectedUser.getUsername()); 
		request.getSession().setAttribute("selectedUser", original);
		return Response.status(200).build();
	}
	
	@GET
	@Path("/loadSelectedUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount selectedUser(@Context HttpServletRequest request) {
		return (UserAccount) request.getSession().getAttribute("selectedUser");
	}*/
	
	@POST
	@Path("/loadDMs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<DM> loadDMs(UserAccount selectedUser, @Context HttpServletRequest request) {
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		ArrayList<DM> dms = loggedUser.getChats().get(selectedUser.getUsername());
		if(dms==null) {
			return new ArrayList<DM>();
		}
		return dms;
	}
	
	@GET
	@Path("/loadUsersFromMyDMs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<UserAccount> loadDMs(@Context HttpServletRequest request) {
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		
		UserAccount original = userDAO.getUserAccountByUsername(loggedUser.getUsername());
		Collection<String> allUser = original.getChats().keySet();
		return userDAO.getUsersByUsernamesList(allUser);
	}
	
	@POST
	@Path("/addDM")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public DM addDM(DM dm, @Context HttpServletRequest request) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		return userDAO.sendDM(dm);
	}
	
	
	
}
