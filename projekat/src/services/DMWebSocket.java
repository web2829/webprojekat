package services;
import java.io.IOException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.DM;
import dao.UserAccountDAO;

@ServerEndpoint("/dmWebsocket/{username}")
public class DMWebSocket {
	
    private static HashMap<String, Session> sessions = new HashMap<>();

    @OnOpen
    public void onOpen(@PathParam("username") String username, Session session) {
        System.out.println("User " + username + " connected on " + session.getId());
        sessions.put(username, session);
    }

    @OnMessage
    public void onMessage(String message, Session session) throws JsonParseException, JsonMappingException, IOException {
    	ObjectMapper o = new ObjectMapper();
    	DM dm = o.readValue(message, DM.class);
    	System.out.println(message);
    	System.out.println(dm);
    	System.out.println(dm.getRecieverUsername());
    	
    	UserAccountDAO userDAO = UserAccountDAO.getUserAccountDAO();
    	dm = userDAO.sendDM(dm);
    	
    	Session receiverSession = sessions.get(dm.getRecieverUsername());
    	
    	if (receiverSession != null) {
    		receiverSession.getBasicRemote().sendText(o.writeValueAsString(dm));	
    	}
    }

    @OnClose
    public void onClose(@PathParam("username") String username, Session session, CloseReason closeReason) {
    	System.out.println("User " + username + " disconected from" + session.getId());
        sessions.remove(username);
    }

    
}
