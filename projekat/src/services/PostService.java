package services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import beans.Post;
import beans.UserAccount;
import dao.AbsPath;
import dao.PostDAO;
import dao.UserAccountDAO;

import org.glassfish.jersey.media.multipart.FormDataParam;  

@Path("/PostService")
public class PostService {

	@Context
	ServletContext ctx;
	
	public PostService() {
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("postDAO") == null) {
			ctx.setAttribute("postDAO", getUserDAO().postDAO);
		}
	}
	
	@POST
	@Path("/newPost")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Post createNewPost(Post post) {
		UserAccount user = getUserDAO().getUserAccountByUsername(post.getUsername());
		System.out.println(user);
		if (user == null) {
			return null;
		}
		
		return getPostDAO().addPost(user, post);
	}
	
	@POST
	@Path("/uploadImage")
	@Consumes(MediaType.MULTIPART_FORM_DATA)  
	@Produces(MediaType.TEXT_PLAIN)
	public Response uploadImage(
			@FormDataParam("file") InputStream uploadedInputStream) {
//		
		String directoryName = AbsPath.absPath + "images";
		
		File directory = new File(directoryName);
	    if (! directory.exists()){
	        System.out.println(directory.mkdir()); 
	    }
		
	    UUID imageId = UUID.randomUUID();
		String fileLocation = directoryName + "\\" + imageId + ".jpg";
		
        //saving file  
		try {  
		    FileOutputStream out = new FileOutputStream(new File(fileLocation));  
		    int read = 0;  
		    byte[] bytes = new byte[1024];  
		    out = new FileOutputStream(new File(fileLocation));  
		    while ((read = uploadedInputStream.read(bytes)) != -1) {  
		        out.write(bytes, 0, read);  
		    }  
		    out.flush();  
		    out.close();  
		    
		    System.out.println("File successfully uploaded to : " + fileLocation);
			
			return Response.status(200).entity(imageId.toString()).build();  
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return Response.status(400).build();
	}
	
	@GET
	@Path("/feed/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeedPosts(@PathParam("username") String username) {
		System.out.println("usao");
		UserAccount user = getUserDAO().getUserAccountByUsername(username);
		List<Post> friendsPosts = new ArrayList<>();
		friendsPosts.addAll(user.getMyPosts());
		
		for (String friendUsername : user.getFriends()) {
			System.out.println(friendUsername);
			UserAccount friend = getUserDAO().getUserAccountByUsername(friendUsername);
			friendsPosts.addAll(friend.getMyPosts());
		}

		for (Post p : friendsPosts){
			System.out.println(p.getText());
		}
		
		friendsPosts = friendsPosts.stream()
				.sorted(Comparator.comparing(p -> getDateFromString(p.getDate())))
				.collect(Collectors.toList());
		
		ResponseBuilder response = Response.status(200).entity((Object) friendsPosts); 
	    
        return response.build();   
	}
	
	private LocalDate getDateFromString(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        return LocalDate.parse(date, formatter);
	}
	
	private PostDAO getPostDAO() {
		return ((PostDAO) ctx.getAttribute("postDAO"));
	}
	
	private UserAccountDAO getUserDAO() {
		return ((UserAccountDAO) ctx.getAttribute("userDAO"));
	}
	
	
	
}