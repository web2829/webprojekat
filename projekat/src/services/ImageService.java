package services;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import dao.AbsPath;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


@Path("/ImageService")
public class ImageService {

	@Context
	ServletContext ctx;
	
	public ImageService() {
	}
	
	@PostConstruct
	public void init() { 
	}
	
	@GET
    @Path("/getImg/{imgId}")
	@Produces("image/jpg")  
	public Response getImg(@PathParam("imgId") String imgId){ 
        File file = new File(AbsPath.absPath + "images\\" + imgId + ".jpg");  
   
        ResponseBuilder response = Response.status(200).entity((Object) file); 
	    
        return response.build();  
	}
}