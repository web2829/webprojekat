package services;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.UserAccount;
import dao.CommentDAO;
import dao.PostDAO;
import dao.UserAccountDAO;

@Path("/LoginService")
public class LoginService {
	
	@Context
	ServletContext ctx;
	
	public LoginService() {
		
	}
	
	@PostConstruct
	public void init() {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		if (userDAO == null) {
	    	userDAO = UserAccountDAO.getUserAccountDAO();
			ctx.setAttribute("userDAO", userDAO);
		}
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(UserAccount user, @Context HttpServletRequest request) {
		UserAccountDAO userDao = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount loggedUser = userDao.find(user.getUsername(), user.getPassword());
		if (loggedUser == null) {
			return Response.status(400).entity("Invalid username and/or password.").build();
		}
		if(loggedUser.isBlocked()) {
			return Response.status(400).entity("You have been blocked by admin.").build();
		}
		request.getSession().setAttribute("user", loggedUser);
		return Response.status(200).entity(loggedUser).build();
	}
	
	
	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void logout(@Context HttpServletRequest request) {
		request.getSession().invalidate();
	}
	
	@POST
	@Path("/block/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void block(@PathParam("username") String username, @Context HttpServletRequest request) {
		UserAccountDAO userDao = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount user = userDao.getUserAccountByUsername(username);
		userDao.blockUser(user);
	}
	@POST
	@Path("/unblock/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void unblock(@PathParam("username") String username, @Context HttpServletRequest request) {
		UserAccountDAO userDao = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount user = userDao.getUserAccountByUsername(username);
		userDao.unblockUser(user);
	}
	
	@GET
	@Path("/currentUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount login(@Context HttpServletRequest request) {
		return (UserAccount) request.getSession().getAttribute("user");
	}
}
