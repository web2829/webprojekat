package services;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Sex;
import beans.UserAccount;
import dao.UserAccountDAO;

@Path("/RegistrationService")
public class RegistrationService {
	
	@Context
	ServletContext ctx;
	
	public RegistrationService() {
		
	}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null) {
			ctx.setAttribute("userDAO", UserAccountDAO.getUserAccountDAO());
		}
	}
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(UserAccount user, @Context HttpServletRequest request) {
		UserAccountDAO userDao = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount userExists = userDao.getUserAccountByUsername(user.getUsername());
		if (userExists != null) {
			return Response.status(400).entity("This username is already taken.").build();
		}
		UserAccount loggedUser = userDao.addUser(user.getFirstName(), 
												 user.getLastName(), 
												 user.getBirthDate(), 
												 user.getSex(), 
												 user.getEmail(), 
												 user.getUsername(), 
												 user.getPassword(),
												 false);
		
		
		request.getSession().setAttribute("user", loggedUser);
		return Response.status(200).entity(loggedUser).build();
	}
	
	@POST
	@Path("/change")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount changeProfile(UserAccount userChanges, @Context HttpServletRequest request) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		return userDAO.changeUser(loggedUser, userChanges);
	}
	
	@GET
	@Path("/currentUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserAccount login(@Context HttpServletRequest request) {
		return (UserAccount) request.getSession().getAttribute("user");
	}
}
