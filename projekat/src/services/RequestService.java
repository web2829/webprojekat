package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Comment;
import beans.DM;
import beans.FriendRequest;
import beans.Post;
import beans.Status;
import beans.UserAccount;
import dao.DMsDAO;
import dao.FriendRequestDAO;
import dao.PostDAO;
import dao.UserAccountDAO;

@Path("/RequestService")
public class RequestService {
	
	@Context
	ServletContext ctx;
	
	public RequestService() {
	}
	
	@PostConstruct
	public void init() {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		if (userDAO == null) {
	    	userDAO = UserAccountDAO.getUserAccountDAO();
			ctx.setAttribute("userDAO", userDAO);
		}
		FriendRequestDAO friendRequestDAO = (FriendRequestDAO) ctx.getAttribute("friendRequestDAO");
		if (friendRequestDAO == null) {
			friendRequestDAO = userDAO.friendRequestDAO;
			ctx.setAttribute("friendRequestDAO", friendRequestDAO);
		}
	}
	
	@GET
	@Path("/getSentRequests")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<FriendRequest> getSentRequests(@Context HttpServletRequest request) {
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		return loggedUser.getPendingSentFriendRequests();	
	}
	
	@POST
	@Path("/cancelRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean cancelRequest(FriendRequest friendRequest, @Context HttpServletRequest request) {
		FriendRequestDAO friendRequestDAO = (FriendRequestDAO) ctx.getAttribute("friendRequestDAO");
		FriendRequest original = friendRequestDAO.getRequestByID(friendRequest.getRequestID());
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		return userDAO.changeRequestStatus(original, Status.CANCELED);
	}
	
	@POST
	@Path("/acceptRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean acceptRequest(FriendRequest friendRequest, @Context HttpServletRequest request) {
		FriendRequestDAO friendRequestDAO = (FriendRequestDAO) ctx.getAttribute("friendRequestDAO");
		FriendRequest original = friendRequestDAO.getRequestByID(friendRequest.getRequestID());
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		return userDAO.changeRequestStatus(original, Status.ACCEPTED);
	}
	
	@POST
	@Path("/rejectRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean rejectRequest(FriendRequest friendRequest, @Context HttpServletRequest request) {
		FriendRequestDAO friendRequestDAO = (FriendRequestDAO) ctx.getAttribute("friendRequestDAO");
		FriendRequest original = friendRequestDAO.getRequestByID(friendRequest.getRequestID());
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		return userDAO.changeRequestStatus(original, Status.REJECTED);
	}
	
	@POST
	@Path("/follow/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public FriendRequest follow(@PathParam("username")String otherUsername, @Context HttpServletRequest request) {
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		return userDAO.sendRequest(loggedUser, otherUsername);
	}
	
	@POST
	@Path("/unfollow/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean unfollow(@PathParam("username")String otherUsername, @Context HttpServletRequest request) {
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		return userDAO.removeFriend(loggedUser, otherUsername);
	}
	
	@GET
	@Path("/isRequestSent/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isRequestSent(@PathParam("username")String otherUsername, @Context HttpServletRequest request) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		return userDAO.isRequestSent(loggedUser, otherUsername);
	}
	
	@GET
	@Path("/mutuals/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<String> getMutuals(@PathParam("username")String otherUsername, @Context HttpServletRequest request) {
		UserAccountDAO userDAO = (UserAccountDAO) ctx.getAttribute("userDAO");
		UserAccount loggedUser = (UserAccount) request.getSession().getAttribute("user");
		return userDAO.mutualFreinds(loggedUser, otherUsername);
	}
}
