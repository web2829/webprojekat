# WebProjekat

Social Media project for University done in in a pair.

We both worked both on Client and Server side - using Java and JavaScript, as well as HTML and CSS.

Implementation of WebSockets for purposes of sending and real-time receiving of messages between users through chat.
