function getEditTextarea(currentText, id){
    return `<textarea id="${id}" class="form-control input-lg p-text-area">${currentText}</textarea>`;
}

function getP(text){
    return `<p>${text}</p>`;
}

function addEditListener(editButtonId, textDivId, textareaId) {  // entityId, postRequestFunc
    let editButton = $("#" + editButtonId);
    let textDiv = $("#" + textDivId);
    let text = $(textDiv).find("p").text();

    $(editButton).click(function(){
        $(textDiv).html(getEditTextarea(text, textareaId));
        let textarea = $("#" + textareaId);
        
        $(textarea).keypress(function(e){
            let key = e.which;

            if (key == 13){ //enter key
                let newText = $(textarea).val();
                //TODO send to back
                $(textDiv).html(getP(newText));
            }
        })
    });
}

$(document).ready(function() {
    // prvo request za post, komentare, lajkove. ..
    addEditListener("edit_post_button", "post_body", "edit_post_textarea");

    for (let i = 0; i < 2; i++){
        let commentId = i;
        let editButtonId = "edit_comment_button_" + commentId;
        let textDivId = "comment_text_" + commentId;
        let textareaId = "edit_comment_textarea_" + commentId;
        htmlComment = getHtmlComment(editButtonId, textDivId);
        let comments = $(".comments");
        comments.append(getHtmlComment(editButtonId, textDivId));
        addEditListener(editButtonId, textDivId, textareaId);
        // napravi event za dugmad
    }

});

function getHtmlComment(editButtonId, textDivId){
    return `
        <div class="comment row post m-1 pt-2">
            <div class="row commenter_info d-flex align-items-center">
                <div class="col-sm-2">
                    <img src="images/profile_picture.jpg" alt="user profile photo" class="rounded-circle" id="comment_user_profile_photo">
                </div>
                <div class="col-sm-7 ">
                    <p id="comment_user_fullname">Name Lastname</p>
                </div>
                <div class="col-sm-3 edit_comment_part">
                    <i class="fas fa-edit" id="${editButtonId}"></i>
                    <i class="fas fa-trash-alt" ></i>
                </div>
            </div>
            <div class="row comment_timestamps">
                <div class="col-sm-6">
                    <p id="edited_comment_timestamp" >e: 22.01.2022. 22:19</p>
                </div>
                <div class="col-sm-6" align="right">
                    <p id="comment_timestamp">20.01.2022. 07:28</p>
                </div>
            </div>
            <div class="row comment_body pt-1" id=${textDivId}>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum, quia.</p>
            </div>
        </div>
    `
    // sredi se html
}